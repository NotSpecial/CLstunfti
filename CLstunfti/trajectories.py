'''
CLstunfti
by Axel Schild

This file is part of CLstunfti.

CLstunfti is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as 
published by the Free Software Foundation, either version 3 of 
the License, or any later version.

CLstunfti is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public 
License along with CLstunfti.  If not, see <http://www.gnu.org/licenses/>.


Here is the <class_trajectories> which contains all functions that are necessary 
for the propagation.
'''
import numpy                        as np
import CLstunfti.conversion_factors as conv
import CLstunfti.ftools             as ftools
from   CLstunfti.tools  import *
from   numpy.random     import rand as nrand
from   copy             import copy

class class_trajectories:
  ''' set of all trajectories starting from the same initial position
      notation: self.g_xxx is a parameter that is equal for all trajectories
                self._xxx  is a parameter that is different for each trajectory
      NOTE:
        For the detector class and for the function count_trajectories below 
        the trajectories are assigned a weight. This is for an integration 
        over the initial depth. 
        Examples (which are already implemented):
          *** Uniform sampling ***
          We have
            int_a^b f(x) dx = sum_{j=1}^n w_j f_j(X_j)
          where X_j is a random variable uniformly distributed within [a,b] and 
          where the weights are 
            w_j = (b-a)/n
          *** Exponential sampling ***
          We have 
            int_0^{inf} f(x) dx = sum_{j=1}^n w_j f_j(X_j)
          where X_j is a random variable distributed according to 
            P(X) = 1/b * exp(-X/b)
          within [a,inf) and where the weights are 
            w_j = 1/(n*P(X_j))
  '''
  def __init__(self, n, F_eff_r=0, F_no_inel=0, F_fixed_r=0, F_no_rdf=1):
    self.F_eff_r       = F_eff_r                     # uses the effective scattering distance 1/r_eff = 1/EMFP + 1/IMFP
                                                     # (has to be an integer; 0 or 1)
    self.F_no_inel     = F_no_inel                   # [DEBUGGING] flag to disable inelastic scattering 
                                                     # (has to be an integer; 0 or 1)
    self.F_fixed_r     = F_fixed_r                   # [DEBUGGING] flag to use fixed scattering distance; disables surface 
                                                     # and inelastic scattering, uses sdat.emfp as scattering distance 
                                                     # (has to be an integer; 0 or 1)
    self.F_no_rdf      = F_no_rdf                    # [UNTESTED] flag to use homogeneous medium instead of pair correlation 
                                                     # function
                                                     # (has to be an integer; 0 or 1)
    self.g_xyz0        = None                        # initial starting position
                                                     # NOTE: different formats are possible, see <initialize>
    self.g_size        = n                           # number of trajectories
    self.g_step        = -1                          # number of the current step
    self.g_n_alive     = n                           # number of trajectories that are still alive
    self.r             = np.zeros([3,n], order='F')  # current Cartesian coordinates in lab frame
    self.dr            = np.zeros([3,n], order='F')  # current displacement
    self.alive         = np.ones(n,dtype=int)        # 0 = dead     , 1 = running
    self.b_alive       = np.ones(n,dtype=bool)       # self.alive as boolean array (defined for computational efficiency)
                                                     # NOTE: This needs to be updated manually before using it!
    self.escaped       = np.zeros(n,dtype=int)       # 0 = in liquid (z<0), 1 = escaped (z>0)
    if not F_fixed_r:
      self.path        = np.zeros(n)                 # path travelled during propagation step (norm of dr)
      self.path_tot    = np.zeros(n)                 # path travelled so far
    if (not F_no_inel) and (not F_eff_r):
      self.path_max    = np.zeros(n)                 # maximum path length for each trajectory
    self.theta         = np.zeros(n)                 # current azimuthal angle for displacement dr (w.r.t. current direction)
                                                     # NOTE: If the trajectory exits, this changes to azimuthal angle of dr in the lab frame.
    self.phi           = np.zeros(n)                 # current polar     angle for displacement dr (w.r.t. current direction)
                                                     # NOTE: If the trajectory exits, this changes to azimuthal angle of dr in the lab frame.
    self.theta_old     = np.zeros(n)                 # last azimuthal angle of current direction dr in lab frame (for rotation to lab frame)
    self.phi_old       = np.zeros(n)                 # last polar     angle of current direction dr in lab frame (for rotation to lab frame)
    self.ion_angle     = 0.                          # additional angle for ionization that simulates a rotated laser polarization axis
    self.weights       = np.ones(n) / n              # weights for trajectories (depend on the sampling of the initial position)
    
  def initialize(self,sdat,mode='given',ion_angle=0.,gamma=0,speakF=False,**kwargs):
    '''
    Initialize trajectory data that needs to be initialized. The surface is at 
    z=0, hence all trajectories should have initial positions with negative 
    z-values.
    
    NOTE: The length unit of the input should be bohr (atomic units).
    
    <mode> can be 
      'given' ......... Initial position is given as <xyz0>. May be a number 
                        (then it is assumed that it is the same initial 
                        z-position for all trajectories, x=y=0), a number array 
                        with 3 entries (same (x,y,z)-position for all 
                        trajectories), or an array with (3,n) entries giving the 
                        initial position of all trajectories.
                        NOTE: Corresponding sampling weigths should be given if 
                        necessary.
      'linear' ........ Random initial position along z sampled uniformly 
                        between <z_min> and <z_max>.
      'exponential' ... Random initial position along z sampled with an 
                        exponential distribution with <z_scale> being the scale 
                        parameter:  1/z_scale * exp(-z/z_scale)
                        The sampling weights are computed.
                        NOTE: z is smaller than zero, this has to be accounted 
                        for when computing the weights.
     
    '''
    
    # initial positions
    if mode == 'given':
      try:    
        xyz0 = kwargs['xyz0']
      except: 
        print(' Variable <xyz0> is missing.')
      # warn about the weights
      if speakF:
        print(' Initial coordinates were provided. Please check   ')
        print(' the weights for the summation of the trajectories.')
      # select type of input
      if isinstance(xyz0,np.ndarray):
        # assuming all trajectories have the same 3d initial coordinates
        if xyz0.shape == (3,):
          self.g_xyz0    = xyz0                      # initial start along z direction
          self.r[0,:]    = self.g_xyz0[0]            # set initial x-value
          self.r[1,:]    = self.g_xyz0[1]            # set initial y-value
          self.r[2,:]    = self.g_xyz0[2]            # set initial z-value
        # assuming all initial z-coordinates are given
        elif xyz0.shape == (self.g_size,):
          self.r[2,:]    = xyz0
        # assuming all initial coordinates are given
        else:
          self.r         = xyz0
      else:
        # assume only one initial z-coordinate for all trajectories is given
        self.r[2,:] = xyz0
      if speakF:
        print(' Initial positions set. Check if you have the correct weights for the trajectories.')
    elif mode == 'linear':
      try:
        z_min = kwargs['z_min']
        z_max = kwargs['z_max']
      except:
        print(' Missing either <z_min> or <z_max>.')
      self.r[0,:]  = np.zeros(self.g_size)
      self.r[1,:]  = np.zeros(self.g_size)
      self.r[2,:]  = z_min + (z_max-z_min) * np.random.rand(self.g_size)
      self.weights = np.ones(self.g_size) * (z_max-z_min) / self.g_size         # we could also use weights 1 here
    elif mode == 'exponential':
      try:
        zs = kwargs['z0_scale']
      except:
        print(' Missing <z0_scale>.')
      initial_depths = -np.random.exponential(scale=zs,size=self.g_size)
      if not gamma:
        self.r[0,:]  = np.zeros(self.g_size)
        self.r[1,:]  = np.zeros(self.g_size)
        self.r[2,:]  = initial_depths
      else:
        self.r[0,:]  = np.zeros(self.g_size)
        self.r[1,:]  = -initial_depths*np.sin(gamma)
        self.r[2,:]  =  initial_depths*np.cos(gamma)
      self.weights = (1./zs * np.exp(self.r[2,:]/zs))**(-1.) / self.g_size      # NOTE the sign, because z<0
      
    else:
      print(' Wrong initialization mode. Set initial positions manually.')
      
    # maximum path length to travel up to inelastic scattering
    # not used if inelastic scattering is disabled, a fixed scattering distance
    # is used of an effective scattering distance for both elastic and inelastic
    # scattering
    if (not self.F_no_inel) and (not self.F_fixed_r) and (not self.F_eff_r):
      self.path_max  = -sdat.imfp * np.log(nrand(self.g_size))
    
    # effective elastic & inelastic scattering distance
    if self.F_eff_r:
      self.ifrac = sdat.imfp / (sdat.emfp+sdat.imfp) # fraction of inelastic scattering
      self.r_eff = sdat.emfp*self.ifrac              # effective scattering length
    
    # additional rotation angle for ionization
    self.ion_angle = ion_angle
    if speakF:
      print(' Setting relative ionization angle to %10.5f' % (self.ion_angle))
    
    if not self.F_no_rdf:
      try:
        sdat.rdf_grid
        sdat.rdf_cdf
      except NameError:
        print(' If you want to use an radial distribution function for \n'
              ' scattering, you need to set the grid and integrated RDF \n'
              ' as variables <rdf_grid> and <rdf_cdf> of class_system.')
    
    # set tilting angle of surface
    self.gamma = gamma
    #if gamma:
      #print('Setting surface tilting angle gamma to %10.5f radian. ' % gamma)
      #print(' Note that the debugging options are not implememnted ')
      #print(' for the tilted surface, i.e., F_fixed_r and F_no_inel')
      #print(' and F_eff_rf use a flat surface only.                ')
    
    return
  
  def ionize(self,sdat,speakF=False):
    '''
    Initial ionization step: determine ionization angles and propagate.
    
    This function is applied to all trajectories, as they should all 
    be alive intially.
    '''
    
    if self.escaped.any(): 
      print(' WARNING: ionize() is called but there are some trajectories  ')
      print('          that still have the <escaped> flag. Make sure to set')
      print('          their <alive> and <escaped> flag to zero before     ')
      print('          calling ionize()                                    ')
    
    # determine polar scattering angle from given distribution
    self.theta = np.interp(nrand(self.g_size),sdat.cdf_ion,sdat.ang_ion)
    
    # determine azimuthal scattering angle from uniform distribution
    self.phi = nrand(self.g_size) * 2 * np.pi - np.pi
      
    # propagate to the next position
    self.propagate(sdat,ionF=1,speakF=speakF)
    
    return

  def propagate(self,sdat,ionF=0,speakF=False):
    '''
    Propagate only alive trajectories.
    
    Needs ionF (integer) to indicate whether it is the propagation step right 
    after the ionization step (ionF = 1) or whether it is a propagation step
    after a scattering event.
    '''
    
    # convenience definition for using self.alive as indices
    self.b_alive = self.alive.astype(bool)
    self.g_n_alive = np.sum(self.alive)
    
    # Save last (normalized) displacement vector.
    if not ionF:
      # store angles of current position vector for backrotation 
      self.theta_old[self.b_alive], \
        self.phi_old[self.b_alive] = cart2sph_ang(self.dr[0,self.b_alive],
                                                  self.dr[1,self.b_alive],
                                                  self.dr[2,self.b_alive])
    
    # propagate
    if self.F_no_rdf:
      if (not self.F_fixed_r) and (not self.F_eff_r):
        # homogeneous scattering with EMFP
        self.path[self.b_alive] = -sdat.emfp * np.log(nrand(self.g_n_alive))
      elif self.F_eff_r:
        self.path[self.b_alive] = -self.r_eff * np.log(nrand(self.g_n_alive))
    else:
      # scattering according to cumulative distribution function obtained from radial distribution function
      #i_r = np.digitize(nrand(self.g_n_alive),sdat.rdf_cdf)
      #self.path[self.b_alive] = sdat.rdf_grid[i_r-1]
      self.path[self.b_alive] = np.interp(nrand(self.g_n_alive),sdat.rdf_cdf,sdat.rdf_grid)
    
    if self.F_fixed_r:
      # propagate with fixed distance given by sdat.emfp (no surface, no inelastic scattering)
      ftools.propagate_fixed(
                          self.r,
                          self.dr,
                          sdat.emfp,
                          ionF,
                          np.where(self.alive)[0],
                          self.theta,
                          self.phi,
                          self.theta_old,
                          self.phi_old,
                          self.ion_angle,
                          self.g_size, 
                          self.g_n_alive)
    else:
      if self.F_no_inel:
        # propagate without inelastic scattering and with surface with stopping at surface
        ftools.propagate_noinel(
                          self.escaped, 
                          self.r, 
                          self.dr,
                          self.path_tot,
                          self.path,
                          ionF,
                          np.where(self.alive)[0],
                          self.theta, 
                          self.phi, 
                          self.theta_old, 
                          self.phi_old, 
                          self.ion_angle,
                          self.g_size, 
                          self.g_n_alive)
      elif self.F_eff_r:
        # propagate with effective scattering distance for elastic and inelastic scattering
        dice = nrand(self.g_n_alive)
        ftools.propagate_r_eff(
                          self.alive,
                          self.escaped, 
                          self.r, 
                          self.dr, 
                          self.path_tot,
                          self.path,
                          self.ifrac,
                          dice,
                          ionF,
                          np.where(self.alive)[0],
                          self.theta, 
                          self.phi, 
                          self.theta_old, 
                          self.phi_old, 
                          self.ion_angle,
                          self.g_size, 
                          self.g_n_alive)
      elif self.gamma:
        # propagate with inelastic scattering and with surface tilted by angle gamma with stopping at surface
        ftools.propagate_tilted_surface(
                          self.gamma,
                          self.alive, 
                          self.escaped, 
                          self.r, 
                          self.dr, 
                          self.path_tot,
                          self.path,
                          ionF,
                          np.where(self.alive)[0], 
                          self.path_max, 
                          self.theta, 
                          self.phi, 
                          self.theta_old, 
                          self.phi_old, 
                          self.ion_angle,
                          self.g_size, 
                          self.g_n_alive)
      else:
        # propagate with inelastic scattering and with surface with stopping at surface
        ftools.propagate(
                          self.alive, 
                          self.escaped, 
                          self.r, 
                          self.dr, 
                          self.path_tot,
                          self.path,
                          ionF,
                          np.where(self.alive)[0], 
                          self.path_max, 
                          self.theta, 
                          self.phi, 
                          self.theta_old, 
                          self.phi_old, 
                          self.ion_angle,
                          self.g_size, 
                          self.g_n_alive)
      self.g_n_alive = np.sum(self.alive)
    
    # update counter
    self.g_step       += 1
    
    return
  
  def scatter(self,sdat,speakF=False):
    ''' 
    Elastic scattering: Determine the scattering angle and propagate.
    '''
    if self.escaped.any(): 
      print(' WARNING: scatter() is called but there are some trajectories ')
      print('          that still have the <escaped> flag. Make sure to set')
      print('          their <alive> and <escaped> flag to zero before     ')
      print('          calling scatter()                                   ')
    
    # convenience definition for using self.state and self.alive as indices
    self.b_alive = self.alive.astype(bool)
    
    # determine polar scattering angle from given distribution
    self.theta[self.b_alive] = np.interp(nrand(self.g_n_alive),sdat.cdf_sca,sdat.ang_sca)
    
    # determine azimuthal scattering angle from uniform distribution
    self.phi[self.b_alive] = nrand(self.g_n_alive) * 2 * np.pi - np.pi
  
    # propagate
    self.propagate(sdat,ionF=0,speakF=speakF)
    
    return
  
  def detect(self,detector,speakF=False):
    '''
    Convenience function to be used with the detector class. Should be run
    after ionize() and scatter() to count trajectories and kill the 
    ones that escaped.
    
    For more flexibility, use count_trajectories() and kill_escaped().
    '''
    
    # NOTE: If the trajectory left the liquid, the angles theta_old, phi_old 
    # represent the angles for the *current* displacement in the lab frame, 
    # unlike in any other situation, where they represent those of the previous 
    # displacement. See ftools.propagate for more information.
    
    # detect the electrons that left the liquid and kill them
    ftools.detect(detector.count,
                  self.alive,
                  np.where(self.escaped)[0],
                  self.weights,
                  self.theta_old,
                  detector.angle,
                  self.g_size,
                  np.sum(self.escaped),
                  detector.size)
    self.g_n_alive = np.sum(self.alive)
    
    # reset escape counter (escaped trajectories were killed, hence this is not really necessary)
    self.escaped.fill(0)
    
    return

  def kill_escaped(self):
    '''
    Kills escaped trajectories. If detect() is not used, this should be run
    after ionize() and scatter() to kill trajectories that exited the liquid
    (otherwise they are propagated further, which is not so clever).
    '''
    self.alive[self.escaped.astype(bool)] = 0   # kill escaped trajectories
    self.escaped.fill(0)                        # set <escaped> flag to zero
    self.g_n_alive = np.sum(self.alive)         # compute number of alive trajectories
    return
  
  def count_trajectories(self,det_angle=None):
    '''
    Count trajectories that escaped under a certain detection angle. Returns 
    the sum of the weights of the counted trajectories.
    
    Input:
      det_angle ... float with 0<det_angle<pi or None or list or numpy array; 
                    if None, all escaped trajectories are counted (default: None)
    Output:
      count ....... sum of weights of counted trajectories
    '''
    b_esc = self.escaped.astype(bool)                 # for selection of escaped trajectories
    if hasattr(det_angle, "__len__") or det_angle:    # count trajectories exited with theta<det_angle
      theta, phi = cart2sph_ang( self.dr[0,b_esc],
                                 self.dr[1,b_esc],
                                 self.dr[2,b_esc])    # theta under which it left the surface
      # check if we need to count for multiple openning angles
      if hasattr(det_angle, "__len__"):
        count = np.array([np.sum(self.weights[b_esc][theta<det_angle[ii]]) for ii in range(len(det_angle))])
      else:
        count = np.sum(self.weights[b_esc][theta<det_angle])
    else:                                                                            
      count = np.sum(self.weights[b_esc])             # count all trajectories
    return count
