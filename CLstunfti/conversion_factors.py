'''
CLstunfti
by Axel Schild

This file is part of CLstunfti.

CLstunfti is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as 
published by the Free Software Foundation, either version 3 of 
the License, or any later version.

CLstunfti is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public 
License along with CLstunfti.  If not, see <http://www.gnu.org/licenses/>.


A list of useful conversion factors and constants.
'''
from numpy import pi

# from SI to au (from Wikipedia)
m_to_au   = 1./5.2917721092e-11                                      # length                                  
J_to_au   = 1./4.35974417e-18                                        # energy
sec_to_au = 1./2.418884326505e-17                                    # time
kg_to_au  = 1./9.10938291e-31                                        # mass
C_to_au   = 1./1.602176565e-19                                       # charge
A_to_au   = C_to_au/sec_to_au
as_to_au  = sec_to_au*1e-18
fs_to_au  = sec_to_au*1e-15

# some other conversions
efield_SI_to_au = kg_to_au * m_to_au / (sec_to_au**3 * A_to_au)
deg_to_rad      = pi/180.

# constants
hbar_SI  = 1.054571800e-34       # hbar
ec_SI    = 1.602176565e-19       # elementary charge
c_SI     = 299792458.            # speed of light
eps_SI   = 8.854187817620e-12    # vacuum permittivity [sec**4 A**2 / (m**3 kg)]

c_au     = c_SI * m_to_au / sec_to_au
eps_au   = 1./(4*pi)             # vacuum permittivity
                                 # test: eps_SI*sec_to_au**4*A_to_au**2/m_to_au**3/kg_to_au

# derived
Hz_to_au  = 1./sec_to_au                                             # frequency
eV_to_au  = ec_SI * J_to_au                                          # energy        
W_to_au   = kg_to_au * m_to_au**2 / sec_to_au**3                     # power
nm_to_au  = m_to_au*1e-9                                             # length
A_to_au   = C_to_au/sec_to_au                                        # current
