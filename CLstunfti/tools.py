'''
CLstunfti
by Axel Schild

This file is part of CLstunfti.

CLstunfti is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as 
published by the Free Software Foundation, either version 3 of 
the License, or any later version.

CLstunfti is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public 
License along with CLstunfti.  If not, see <http://www.gnu.org/licenses/>.


Som useful tools.
'''

from   __future__    import  division
from   numpy         import arctan2, sqrt
from   scipy.special import jn
import numpy    as np
import numexpr  as ne
import scipy.io as sio
import h5py
import os

class cd:
  """Context manager for changing the current working directory"""
  def __init__(self, newPath):
    self.newPath = os.path.expanduser(newPath)
  def __enter__(self):
    self.savedPath = os.getcwd()
    os.chdir(self.newPath)
  def __exit__(self, etype, value, traceback):
    os.chdir(self.savedPath)

def saveToHDF5(fname,stuff,appendF=True,addF=False):
  '''
  Improved version of the save function that emphasizes the "Hierarchical" in
  the hierarchical dataformat. 
  
  Input:
    fname ..... file name (if not ending with '.h5', this will be appended, 
                unless appendF == False)
    stuff ..... dictionary with the data to be written
    appendF ... if True, append '.h5' automatically
    addF ...... add to existing file
  
  Dictionary <stuff> should be organized as follows:
    
    stuff = {'n1':d1,'n2':d2} will write d1 to '/n1' and d2 to '/n2'; both 
    n1 and n2 can be paths with subgroups, e.g. n1 = 'group/subgroup/data_name'
    
    stuff = {'g1':{'n1':d1,'n2':d2}} will write d1 to '/g1/n1' and d2 to 
    '/g1/n2'
    
    stuff = {'g1':{'s1':{'n1':d1,'n2':d2}} } will write d1 to '/g1/s1/n1' and 
    d2 to '/g1/s1/n2'
    
    etc., and mixtures are of course also possible.
  
  '''
  if appendF:
    if not fname.endswith('.h5'): 
      fname += '.h5'
  
  if addF:
    f = h5py.File(fname, 'a')
  else:
    f = h5py.File(fname, 'w')
  saveToHDF5_helper(f,stuff)
  f.close()
  
def saveToHDF5_helper(f,stuff):
  '''
  Helper function for saveToHDF5 that is called recursively to write the data
  hierarchically.
  '''
  for name, ding in stuff.items():
    if isinstance(ding, dict):
      grp = f.create_group(name)
      saveToHDF5_helper(grp,ding)
    else:
      ding = np.squeeze(ding)
      if len(np.shape(ding)) == 1:
        dset = f.create_dataset(name,(1,len(ding)),data=ding)
      else:
        dset = f.create_dataset(name,(np.shape(ding)),data=ding)

def loadFromHDF5(fname,stuffList=[],appendF=True):
  '''
  Load all elements of the dictionary stuffList or all elements from the HDF5 
  file. Note that in stuffList the complete path needs to be given. Also, note 
  that for a hierarchical structure, the returned dictionary looks like the 
  corresponding input of saveToHDF5.
  
  TODO: Add functionality to load only a group.
  
  Use
   for key,val in data.items(): exec(key + '=val')
  to load all of them in the current workspace.
  '''
  if appendF:
    if not fname.endswith('.h5'):
      fname += '.h5'
  
  f = h5py.File(fname, 'r')
  returnList = {}
  if stuffList:
    for name in stuffList:
      returnList[name] = np.squeeze(f[name][...])
  else:
    loadFromHDF5_helper(returnList,f)
  f.close()
  return returnList

def loadFromHDF5_helper(returnList,f):
  '''
  Helper function for loadFromHDF5 that is called recursively to load the data
  hierarchically.
  '''
  for (name,ding) in f.items():
    if isinstance(ding, h5py._hl.group.Group):
      returnList[name] = {}
      loadFromHDF5_helper(returnList[name],ding)
    else:
      returnList[name] = np.squeeze(ding[...])

def saveToMAT(fname,stuff,appendF=True):
  '''
  Super trivial function to save to Matlab mat file.
  '''
  sio.savemat(fname,stuff,appendmat=appendF)
  
def loadFromMAT(fname,appendF=True,vnames=None):
  '''
  Super trivial function to load from Matlab mat file.
  '''
  if vnames:
    return sio.loadmat(fname,appendmat=appendF,squeeze_me=True,variable_names=vnames)
  else:
    return sio.loadmat(fname,appendmat=appendF,squeeze_me=True)

def ETA(status,problemSize,timePassed,endF=False,endGuessF=True,skipF=False):
  '''
  Simple Estimated Time of Arrival function
  INPUT:
    status ........ current step
    problemSize ... total number of steps
    timePassed .... time used so far
    endF .......... indicate if this is the last step (see below)
    endGuessF ..... indicate if laststep should be guessed
    skipF ......... indicate if text should be replaced or new line each time
  NOTE:
    By default, a call of ETA will overwrite what is written in the current 
    line. Thus, if ETA is called  successively it will update the estimated 
    time of arrival continuously. If you want to write something between each
    call of ETA, use skipF=True to write the output in a new line without 
    text replacement. By default, ETA will try to guess if the last step is 
    reached and will continue to a new line. If you don't want this, set
    endGuessF=False and set endF=True for the last call of ETA.
  '''
  fraction = status/float(problemSize)
  timeEstimated = timePassed/fraction
  timeToGo = timeEstimated-timePassed
  if skipF:
    sys.stdout.write(\
      '\nDone %(a)5.1f%% using %(b)f minutes lifetime. ETA: %(c)f Minutes'\
      % {'a':fraction*100,'b':timePassed/60.,'c':timeToGo/60.})
  else:
    sys.stdout.write(\
      '\rDone %(a)5.1f%% using %(b)f minutes lifetime. ETA: %(c)f Minutes'\
      % {'a':fraction*100,'b':timePassed/60.,'c':timeToGo/60.})
  sys.stdout.flush()
  if endF or (endGuessF and (1-fraction < 1e-3)):
    print('\n')

def find_index(a,b,method=1):
  ''' 
      Find index ob all elements of b in a. 
      Code from 
      http://stackoverflow.com/questions/8251541/numpy-for-every-element-in-one-array-find-the-index-in-another-array
      
      There are two different methods that may be used. They seem to be 
      equivalent in terms of speed and functionality.
  '''
  if method:
    a_sorted = np.argsort(a)
    b_index  = np.searchsorted(a[a_sorted], b)
    return a_sorted[b_index]
  else:
    a_index  = np.argsort(a)
    a_sorted = a[a_index]
    s_index  = np.searchsorted(a_sorted, b)
    b_index  = np.take(a_index, s_index, mode="clip")
    #mask = a[b_index] != b
    return a[b_index]

def rotate_back(vec,theta,phi):
  '''
  Rotate back to reference frame. This has to be done after each scattering.
  
  The function applies the rotation matrix for phi around y on vector vec, then 
  the rotation matrix for theta around z; of course this is done in one step. 
  Thus, it computes np.dot( np.dot( Rz(phi), Ry(theta) ), v ) for each v of vec.
  
  Rotation matrices:
    Rz(phi)   = [[ cp,-sp,  0], [ sp, cp,  0], [  0,  0,  1]]
    Ry(theta) = [[ ct,  0, st], [  0,  1,  0], [-st,  0, ct]]
  '''
  ct, cp = np.cos(theta), np.cos(phi)
  st, sp = np.sin(theta), np.sin(phi)
  return np.vstack([ cp*ct*vec[0] - sp*vec[1] + cp*st*vec[2],
                     sp*ct*vec[0] + cp*vec[1] + sp*st*vec[2],
                    -   st*vec[0]             +    ct*vec[2] ])

def cart2sph_mine(x,y,z):
  '''
  Transform Cartesian coordinates x, y, z (that may be arrays) to spherical
  coordinates r, t, p according to  ISO 80000-2:2009.
  '''
  r = np.sqrt(x**2 + y**2 + z**2)
  p = np.arctan2(y,x)
  t = np.arccos(z/r)
  return r, t, p

def cart2sph(x,y,z, ceval=ne.evaluate):
    '''
    Slighlty modified code from 
    https://stackoverflow.com/questions/4116658/faster-numpy-cartesian-to-spherical-coordinate-conversion/30185737#30185737
    to conform to ISO 80000-2:2009.
    
    ceval = eval            :  Numpy
          = numexpr.evaluate:  Numexpr 
    '''
    p   = ceval('arctan2(y,x)')
    xy2 = ceval('x**2 + y**2')
    t   = ceval('arctan2(z, sqrt(xy2))')
    r   = eval('sqrt(xy2 + z**2)')
    return r, np.pi/2.-t, np.squeeze(p)

def cart2sph_ang(x,y,z, ceval=ne.evaluate):
    '''
    Slighlty modified code from 
    https://stackoverflow.com/questions/4116658/faster-numpy-cartesian-to-spherical-coordinate-conversion/30185737#30185737
    to conform to ISO 80000-2:2009.
    
    ceval = eval            :  Numpy
          = numexpr.evaluate:  Numexpr 
    '''
    p   = ceval('arctan2(y,x)')
    xy2 = ceval('x**2 + y**2')
    t   = ceval('arctan2(z, sqrt(xy2))')
    return np.pi/2.-t, np.squeeze(p)

def sph2cart(r,t,p):
  '''
  Transform Sperical coordinates r, t, p (that may be arrays) to Cartesian
  coordinates x, y, z according to  ISO 80000-2:2009, where r \in (0,infty) is 
  the radial distance, t \in (0,pi) is the angle from the z-axis, and p \in 
  (0,2pi) is the polar angle.
  '''
  x = r * np.sin(t) * np.cos(p)
  y = r * np.sin(t) * np.sin(p)
  z = r * np.cos(t)
  return x, y, z

def bessel_gen(n,u,v,err=1e-12):
  '''
  Generalized Bessel functions.
    err ... maximal relative error
  '''
  k = 0
  result = jn(n-2*k,u) * jn(k,v)
  reldiff = 1.
  while reldiff > err:
    result_old = result
    # first run
    k += 1
    result += jn(n-2*k,u) * jn( k,v)
    result += jn(n+2*k,u) * jn(-k,v)
    # second run
    k += 1
    result += jn(n-2*k,u) * jn( k,v)
    result += jn(n+2*k,u) * jn(-k,v)
    # check relative difference
    reldiff = np.max((result-result_old)/result)
  return result

def eal(z,a,b):
  '''
  Function to be used to obtain the effective attenuation length (EAL), see 
  example.
    z ... starting depth (positive!)
    a ... overall scaling factor
    b ... EAL
  '''
  return a*np.exp(-z/b)

def pad(t,m,b2):
  '''
  Photoelectron angular distribution (PAD) for one-photon ionization of an
  isotropic gas of atoms or molecules.
    t  ... polar angle theta
    m  ... overall scaling factor
    b2 ... coefficient beta_2 of Legendre polynomial P_2
  '''
  c = np.cos(t)
  pad = m * (1. + b2/2.*(3*c**2-1))
  return pad

def pad_chiral(t,m,b1,b2):
  '''
  Photoelectron angular distribution (PAD) for one-photon ionization of an
  isotropic gas of molecules with chirality.
    t  ... polar angle theta
    m  ... overall scaling factor
    b1 ... coefficient beta_2 of Legendre polynomial P_1
    b2 ... coefficient beta_2 of Legendre polynomial P_2
  '''
  c = np.cos(t)
  pad = m * (1. + b1*c + b2/2.*(3*c**2-1))
  return pad

