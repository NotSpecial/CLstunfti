import numpy                                            as np
from   scipy.optimize         import curve_fit
import CLstunfti.conversion_factors                     as conv
import CLstunfti.tools                                  as tools
from   CLstunfti.system       import class_system
from   CLstunfti.trajectories import class_trajectories
# turn on plotting without X server
import matplotlib as mpl
mpl.use('Agg')
import matplotlib.pyplot as plt

def compute_pad(ion_ang_min,ion_ang_max,ion_ang_pts,z0_scale,
                n_traj,sdat,theta_detect,n_runs=1,fname=None,F_speak=False,
                plot_name=False,F_eff_r=0):
  '''
    Convenience function to compute the PAD (photoelectron angular distribution). 
    Input should be in atomic units.
    The laser polarization angle (ionization direction) is sampled linearly 
    between ion_ang_min and ion_ang_max with ion_ang_pts points. The average 
    over the initial positions is done with an exponential distribution of 
    width parameter z0_scale.
    
    Input:
      ion_ang_min ......... minimum ionization angle (in radian)
      ion_ang_max ......... maximum ionization angle (in radian)
      ion_ang_pts ......... number of points for the ionization angle
      z0_scale ............ width parameter of exponential distribution for 
                            sampling of the initial position (in bohr)
      n_traj .............. number of trajectories per run
      sdat ................ system data (instance of class_system)
      theta_detect ........ detection angle (number, numpy array or None (count all trajectories))
      n_runs .............. number of runs; used here because the PAD converges
                            relatively slowly; more runs with less trajectories 
                            yields the same result with less memory demand
      fname ............... file name; if set, this writes the data to the file
                            during each of the n_runs runs
      F_speak ............. flag for some printed comments
    
    Returns:
      ion_ang_grid ........ grid of ionization angles
      counter ............. (weighted) counted trajectories for each detection angle
      fitres .............. result of the fit to tools.pad
      fiterr .............. covariance matrix of the fit to tools.pad
  '''
  # *** INITIALIZATION SECTION ***
  
  # grid of ionization angle
  ion_ang_grid = np.linspace(ion_ang_min,ion_ang_max,ion_ang_pts)

  # prepare counter for exited electrons
  if hasattr(theta_detect, "__len__"): 
    nt = len(theta_detect)
  else: 
    nt = 1
  counter = np.zeros([ion_ang_pts,nt])
  
  # *** PROPAGATION SECTION ******************************************************
  # loop over the number of runs
  for irun in range(n_runs):
    if F_speak: print( ' Doing run %d out of %d.' % (irun+1,n_runs))
    # loop over the number of ionization angles
    for iia, ion_ang in enumerate(ion_ang_grid):
      if F_speak: print( '  Doing angle %d out of %d.' % (iia+1,ion_ang_pts))
      # initialize
      tset = class_trajectories(n_traj,F_eff_r=F_eff_r)
      tset.initialize(sdat,mode='exponential',ion_angle=ion_ang,z0_scale=z0_scale)
      # ionize
      tset.ionize(sdat)
      # count trajectories
      counter[iia] += tset.count_trajectories(theta_detect)
      # kill escaped trajectories
      tset.kill_escaped()
      
      while tset.g_n_alive:
        # scatter
        tset.scatter(sdat)
        # count trajectories
        counter[iia] += tset.count_trajectories(theta_detect)
        # kill escaped trajectories
        tset.kill_escaped()
    
  # fitting to determine the PAD
  fitres, fiterr = [], []
  for ii in range(nt):
    counter_now = counter[:,ii] / counter[:,ii].max()            # normalize
    # decent initial values
    b0 = (counter_now[0] - counter_now[-1]) / (counter_now[0]/2 + counter_now[-1])
    m0 = counter_now[0] / (1.+b0)
    [fres, ferr] = curve_fit(tools.pad, ion_ang_grid, counter_now, np.array([m0,b0]))
    fitres.append(fres)
    fiterr.append(ferr)
  
    if plot_name:
      ion_ang_grid_interp = np.linspace(ion_ang_grid[0],ion_ang_grid[-1],1000)
      plt.figure()
      plt.plot(ion_ang_grid, counter_now,'o')
      plt.plot(ion_ang_grid_interp,tools.pad(ion_ang_grid_interp,*fres))
      plt.savefig('pad_%s_%d.png' % (plot_name,ii))
      plt.close()
  
  if fname:
    stuff = {'ion_ang_grid': ion_ang_grid,
              'counter':      np.squeeze(counter),
              'fitres':       np.squeeze(fitres),
              'fiterr':       np.squeeze(fiterr),
              'irun':         irun,
              'z0_scale':     z0_scale,
              'emfp':         sdat.emfp,
              'imfp_factor':  sdat.imfp/sdat.emfp,
              'theta_detect': theta_detect}
    tools.saveToHDF5(fname,stuff)
    
  return ion_ang_grid, np.squeeze(counter), np.squeeze(fitres), np.squeeze(fiterr)

def compute_pad_tilted_surface(gamma,
                ion_ang_min,ion_ang_max,ion_ang_pts,z0_scale,
                n_traj,sdat,theta_detect,n_runs=1,fname=None,F_speak=False,
                plot_name=False,F_eff_r=0):
  '''
    Convenience function to compute the PAD (photoelectron angular distribution). 
    Input should be in atomic units.
    The laser polarization angle (ionization direction) is sampled linearly 
    between ion_ang_min and ion_ang_max with ion_ang_pts points. The average 
    over the initial positions is done with an exponential distribution of 
    width parameter z0_scale.
    
    Input:
      ion_ang_min ......... minimum ionization angle (in radian)
      ion_ang_max ......... maximum ionization angle (in radian)
      ion_ang_pts ......... number of points for the ionization angle
      z0_scale ............ width parameter of exponential distribution for 
                            sampling of the initial position (in bohr)
      n_traj .............. number of trajectories per run
      sdat ................ system data (instance of class_system)
      theta_detect ........ detection angle (number, numpy array or None (count all trajectories))
      n_runs .............. number of runs; used here because the PAD converges
                            relatively slowly; more runs with less trajectories 
                            yields the same result with less memory demand
      fname ............... file name; if set, this writes the data to the file
                            during each of the n_runs runs
      F_speak ............. flag for some printed comments
    
    Returns:
      ion_ang_grid ........ grid of ionization angles
      counter ............. (weighted) counted trajectories for each detection angle
      fitres .............. result of the fit to tools.pad
      fiterr .............. covariance matrix of the fit to tools.pad
  '''
  # *** INITIALIZATION SECTION ***
  
  # grid of ionization angle
  ion_ang_grid = np.linspace(ion_ang_min,ion_ang_max,ion_ang_pts)

  # prepare counter for exited electrons
  if hasattr(theta_detect, "__len__"): 
    nt = len(theta_detect)
  else: 
    nt = 1
  counter = np.zeros([ion_ang_pts,nt])
  
  # *** PROPAGATION SECTION ******************************************************
  # loop over the number of runs
  for irun in range(n_runs):
    if F_speak: print( ' Doing run %d out of %d.' % (irun+1,n_runs))
    # loop over the number of ionization angles
    for iia, ion_ang in enumerate(ion_ang_grid):
      if F_speak: print( '  Doing angle %d out of %d.' % (iia+1,ion_ang_pts))
      # initialize
      tset = class_trajectories(n_traj,F_eff_r=F_eff_r)
      tset.initialize(sdat,gamma=gamma,mode='exponential',ion_angle=ion_ang,z0_scale=z0_scale)
      # ionize
      tset.ionize(sdat)
      # count trajectories
      counter[iia] += tset.count_trajectories(theta_detect)
      # kill escaped trajectories
      tset.kill_escaped()
      
      while tset.g_n_alive:
        # scatter
        tset.scatter(sdat)
        # count trajectories
        counter[iia] += tset.count_trajectories(theta_detect)
        # kill escaped trajectories
        tset.kill_escaped()
    
  # fitting to determine the PAD
  fitres, fiterr = [], []
  for ii in range(nt):
    counter_now = counter[:,ii] / counter[:,ii].max()            # normalize
    # decent initial values
    b0 = (counter_now[0] - counter_now[-1]) / (counter_now[0]/2 + counter_now[-1])
    m0 = counter_now[0] / (1.+b0)
    [fres, ferr] = curve_fit(tools.pad, ion_ang_grid, counter_now, np.array([m0,b0]))
    fitres.append(fres)
    fiterr.append(ferr)
  
    if plot_name:
      ion_ang_grid_interp = np.linspace(ion_ang_grid[0],ion_ang_grid[-1],1000)
      plt.figure()
      plt.plot(ion_ang_grid, counter_now,'o')
      plt.plot(ion_ang_grid_interp,tools.pad(ion_ang_grid_interp,*fres))
      plt.savefig('pad_%s_%d.png' % (plot_name,ii))
      plt.close()
  
  if fname:
    stuff = {'ion_ang_grid': ion_ang_grid,
              'counter':      np.squeeze(counter),
              'fitres':       np.squeeze(fitres),
              'fiterr':       np.squeeze(fiterr),
              'irun':         irun,
              'z0_scale':     z0_scale,
              'emfp':         sdat.emfp,
              'imfp_factor':  sdat.imfp/sdat.emfp,
              'theta_detect': theta_detect}
    tools.saveToHDF5(fname,stuff)
    
  return ion_ang_grid, np.squeeze(counter), np.squeeze(fitres), np.squeeze(fiterr)

def compute_pad_restart(counter,ion_ang_min,ion_ang_max,ion_ang_pts,z0_scale,
                        n_traj,sdat,theta_detect,F_speak=False,F_eff_r=0):
  '''
    Convenience function to compute the PAD (photoelectron angular distribution). 
    Input should be in atomic units.
    
    NOTE: This function works similiar to compute_pad, except that only one 
    run is made and the initial counter of the trajectories has to be provided.
    The idea is that the loop over several runs can be made in the code calling
    this function.
    
    The laser polarization angle (ionization direction) is sampled linearly 
    between ion_ang_min and ion_ang_max with ion_ang_pts points. The average 
    over the initial positions is done with an exponential distribution of 
    width parameter z0_scale.
    
    Input:
      counter ............. initial (weighted) counter of trajectories; has to 
                            have the same shape as theta_detect (except if 
                            theta_detect=None, then it should be a number)
      ion_ang_min ......... minimum ionization angle (in radian)
      ion_ang_max ......... maximum ionization angle (in radian)
      ion_ang_pts ......... number of points for the ionization angle
      z0_scale ............ width parameter of exponential distribution for 
                            sampling of the initial position (in bohr)
      n_traj .............. number of trajectories per run
      sdat ................ system data (instance of class_system)
      theta_detect ........ detection angle (number, numpy array or None (count all trajectories))
      F_speak ............. flag for some printed comments
    
    Returns:
      ion_ang_grid ........ grid of ionization angles
      counter ............. updated (weighted) counted trajectories for each detection angle
      fitres .............. result of the fit to tools.pad
      fiterr .............. covariance matrix of the fit to tools.pad
  '''
  # *** INITIALIZATION SECTION ***
  # grid of ionization angle
  ion_ang_grid = np.linspace(ion_ang_min,ion_ang_max,ion_ang_pts)

  # prepare counter for exited electrons
  if hasattr(theta_detect, "__len__"): 
    nt = len(theta_detect)
  else: 
    nt = 1
  
  # *** PROPAGATION SECTION ******************************************************
  # loop over the number of ionization angles
  for iia, ion_ang in enumerate(ion_ang_grid):
    if F_speak: print( '  Doing angle %d out of %d.' % (iia+1,ion_ang_pts))
    # initialize
    tset = class_trajectories(n_traj,F_eff_r=F_eff_r)
    tset.initialize(sdat,mode='exponential',ion_angle=ion_ang,z0_scale=z0_scale)
    # ionize
    tset.ionize(sdat)
    # count trajectories
    counter[iia] += tset.count_trajectories(theta_detect)
    # kill escaped trajectories
    tset.kill_escaped()
    
    while tset.g_n_alive:
      # scatter
      tset.scatter(sdat)
      # count trajectories
      counter[iia] += tset.count_trajectories(theta_detect)
      # kill escaped trajectories
      tset.kill_escaped()

  # fitting to determine the PAD
  fitres, fiterr = [], []
  for ii in range(nt):
    counter_now = counter[:,ii] / counter[:,ii].max()            # normalize
    # decent initial values
    b0 = (counter_now[0] - counter_now[-1]) / (counter_now[0]/2 + counter_now[-1])
    m0 = counter_now[0] / (1.+b0)
    [fres, ferr] = curve_fit(tools.pad, ion_ang_grid, counter_now, np.array([m0,b0]))
    fitres.append(fres)
    fiterr.append(ferr)
    
  return ion_ang_grid, np.squeeze(counter), np.squeeze(fitres), np.squeeze(fiterr)

def compute_pad_linear(counter,ion_ang_min,ion_ang_max,
                        ion_ang_pts,z0_delta,n_traj,sdat,theta_detect,
                        F_speak=False,F_eff_r=0):
  # *** INITIALIZATION SECTION ***
  
  # grid of ionization angle
  ion_ang_grid = np.linspace(ion_ang_min,ion_ang_max,ion_ang_pts)

  # prepare counter for exited electrons
  if hasattr(theta_detect, "__len__"): 
    nt = len(theta_detect)
  else: 
    nt = 1
  
  # *** PROPAGATION SECTION ******************************************************
  # loop over the number of ionization angles
  for iia, ion_ang in enumerate(ion_ang_grid):
    if F_speak: print( '  Doing angle %d out of %d.' % (iia+1,ion_ang_pts))
    # set some initial values
    z0          = 0.
    counter_now = np.array(1.0)
    # go deeper as long as there is something counted
    while counter_now.any():
      # update starting position
      z0 -= z0_delta
      # initialize
      tset = class_trajectories(n_traj,F_eff_r=F_eff_r)
      tset.initialize(sdat,mode='given',ion_angle=ion_ang,xyz0=z0)
      # ionize
      tset.ionize(sdat)
      # count trajectories
      counter_now = tset.count_trajectories(theta_detect)
      # kill escaped trajectories
      tset.kill_escaped()
      
      while tset.g_n_alive:
        # scatter
        tset.scatter(sdat)
        # count trajectories
        counter_now += tset.count_trajectories(theta_detect)
        # kill escaped trajectories
        tset.kill_escaped()
      
      counter[iia] += counter_now
      
      if F_speak: 
        print(' Done z0=%10.5f, counted:' % z0)
        print(counter_now)

  # fitting to determine the PAD
  fitres, fiterr = [], []
  for ii in range(nt):
    counter_now = counter[:,ii] / counter[:,ii].max()            # normalize
    # decent initial values
    b0 = (counter_now[0] - counter_now[-1]) / (counter_now[0]/2 + counter_now[-1])
    m0 = counter_now[0] / (1.+b0)
    [fres, ferr] = curve_fit(tools.pad, ion_ang_grid, counter_now, np.array([m0,b0]))
    fitres.append(fres)
    fiterr.append(ferr)
    
  return ion_ang_grid, np.squeeze(counter), np.squeeze(fitres), np.squeeze(fiterr)

def compute_pad_exp_nsca(counter,ion_ang_min,ion_ang_max,ion_ang_pts,z0_scale,
                         n_traj,sdat,theta_detect,n_sca,F_eff_r=0):
  '''
     Same as compute_pad but results separated according to the number of 
     scatterings.
  '''
  
  # *** INITIALIZATION SECTION ***
  # grid of ionization angle
  ion_ang_grid = np.linspace(ion_ang_min,ion_ang_max,ion_ang_pts)
  
  # test counter for exited electrons
  if counter.shape != (ion_ang_pts,n_sca+1):
    print(' Wrong shape of the counter. Should be (ion_ang_pts,n_sca+1)')
  
  counter_dummy = np.zeros(counter.shape)
  
  # *** PROPAGATION SECTION ****************************************************
  for iia, ion_ang in enumerate(ion_ang_grid):
    # initialize
    tset = class_trajectories(n_traj,F_eff_r=F_eff_r)
    tset.initialize(sdat,mode='exponential',ion_angle=ion_ang,z0_scale=z0_scale)
    # ionize
    tset.ionize(sdat)
    # count trajectories
    counter_dummy[iia,0] += tset.count_trajectories(theta_detect)
    # kill escaped trajectories
    tset.kill_escaped()
    
    for iis in range(n_sca):
      # scatter
      tset.scatter(sdat)
      # count trajectories
      counter_dummy[iia,iis+1] = counter_dummy[iia,iis] + tset.count_trajectories(theta_detect)
      # kill escaped trajectories
      tset.kill_escaped()
  
  counter += counter_dummy
  
  # fitting to determine the PAD
  fitres, fiterr = [], []
  for iis in range(n_sca+1):
    counter_now = counter[:,iis]/np.max(counter[:,iis])
    try:
      # decent initial values
      b0 = (counter_now[0] - counter_now[-1]) / (counter_now[0]/2 + counter_now[-1])
      m0 = counter_now[0] / (1.+b0)
      [fres, ferr] = curve_fit(tools.pad, ion_ang_grid, counter_now, np.array([m0,b0]))
    except:
      print(' Fitting failed.')
      fres = np.zeros(2)
      ferr = np.zeros([2,2])
    fitres.append(fres)
    fiterr.append(ferr)
  
  return ion_ang_grid, counter, np.array(fitres), np.array(fiterr)

def extrapolate_imfp_f_pad(sdat,beta_target,emfp,imfp_f,imfp_f_acc,ia_min,ia_max,ia_pts,
                           z0_scale,n_traj_pad,ang_det_pad,n_runs_pad,nmult=5,
                           F_speak=False,F_sym=True,plot_name=False,F_eff_r=0):
  '''
    Given a target EAL and beta value, and given an EMFP, compute a value
    for the average number of elastic scatterings <imfp_f> (equivalent to
    IMFP/EMFP) based on a linear extrapolation of two calculations of the PAD.
  '''
  
  # set the two <imfp_f> that will be used
  if F_sym:
    imfp_f_0 = imfp_f - imfp_f_acc/2.
    imfp_f_1 = imfp_f + imfp_f_acc/2.
  if not F_sym or imfp_f_0<0:
    imfp_f_0 = imfp_f
    imfp_f_1 = imfp_f + imfp_f_acc
  
  # *** first run
  # set EMFP and IMFP
  sdat.emfp = emfp
  sdat.imfp = imfp_f_0 * sdat.emfp
  # compute first beta
  ia_grid, counter, fitres, fiterr = compute_pad(ia_min,ia_max,ia_pts,z0_scale,
                                                 n_traj_pad,sdat,ang_det_pad,n_runs_pad,
                                                 F_speak=F_speak,plot_name=plot_name,
                                                 F_eff_r=F_eff_r)
  beta_0 = fitres[1]
  if F_speak: print('  First  beta: %10.5f' % beta_0)
  
  # *** second run
  # set EMFP and IMFP
  sdat.emfp = emfp
  sdat.imfp = imfp_f_1 * sdat.emfp
  # compute second beta
  ia_grid, counter, fitres, fiterr = compute_pad(ia_min,ia_max,ia_pts,z0_scale,
                                                 n_traj_pad,sdat,ang_det_pad,n_runs_pad,
                                                 F_speak=F_speak,plot_name=plot_name,
                                                 F_eff_r=F_eff_r)
  beta_1 = fitres[1]
  if F_speak: print('  Second beta: %10.5f' % beta_1)
  
  # check: beta computed with larger IMFP/EMFP should always be smaller
  if beta_1 > beta_0:
    print(' WARNING: Error while computing beta.                           ')
    print('          Trying once again. If it fails, try computation with  ')
    print('          more trajectories n_traj_pad or with different initial')
    print('          values.                                               ')
    # repeat calculation with significantly larger step size
    imfp_f_1  = imfp_f + nmult*imfp_f_acc
    sdat.emfp = emfp
    sdat.imfp = imfp_f_1 * sdat.emfp
    ia_grid, counter, fitres, fiterr = compute_pad(ia_min,ia_max,ia_pts,z0_scale,
                                                   n_traj_pad,sdat,ang_det_pad,n_runs_pad,
                                                   F_speak=F_speak,plot_name=plot_name,
                                                   F_eff_r=F_eff_r)
    beta_1 = fitres[1]
    if F_speak: print('  Second beta: %10.5f' % beta_1)
    
  # *** linear interpolation/extrapolation to find the new IMFP/EMFP
  imfp_f = imfp_f_0 + (beta_target-beta_0) * (imfp_f_1-imfp_f_0) / (beta_1-beta_0)
  
  # print results of first run (not the latest value, but it saves computing time 
  # and still shows the convergence)
  print(' EMFP: %8.3f nm, IMFP/EMFP: %8.3f, beta: %8.3f' % (emfp/conv.nm_to_au,imfp_f_0,beta_0))
  
  return imfp_f
