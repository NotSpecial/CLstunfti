import numpy                                            as np
from   scipy.optimize         import curve_fit
import CLstunfti.conversion_factors                     as conv
import CLstunfti.tools                                  as tools
from   CLstunfti.system       import class_system
from   CLstunfti.trajectories import class_trajectories
# turn on plotting without X server
import matplotlib as mpl
mpl.use('Agg')
import matplotlib.pyplot as plt
from time import time

def compute_eal(z0_min,z0_max,z0_pts,n_traj,sdat,theta_detect,n_runs=1,
                F_speak=False,plot_name=False,F_eff_r=0,fit_guess=np.array([1.,1.])):
  '''
    Convenience function to compute the EAL (effective attenuation length). 
    Input should be in atomic units.
    The initial position is sampled linearly between z0_min and z0_max with 
    z0_pts points. Remember that z=0 is the surface, so 
      z0_min < z0_max < 0
    
    Input:
      z0_min ......... minimum starting position (maximum depth, less than 0; in bohn)
      z0_max ......... maximum starting position (minimum depth, less than 0; in bohn)
      z0_pts ......... number of points for the starting position
      n_traj ......... number of trajectories per run
      sdat ........... system data (instance of class_system)
      theta_detect ... detection angle (number, numpy array or None (count all trajectories))
      n_runs ......... number of runs
      F_speak ........ flag for some printed comments
      plot_name ...... false for not plotting, otherwise filename to store plot
    
    Returns:
      z0_grid ........ grid of initial positions
      counter ........ (weighted) counted trajectories for each detection angle
      fitres ......... result of the fit to tools.eal
      fiterr ......... covariance matrix of the fit to tools.eal
  '''
  # *** INITIALIZATION SECTION *************************************************
  # grid of initial positions
  z0_grid = np.linspace(z0_min,z0_max,z0_pts)
  
  # prepare counter for exited electrons
  if hasattr(theta_detect, "__len__"): 
    nt = len(theta_detect)
  else: 
    nt = 1
  counter = np.zeros([z0_pts,nt])

  # *** PROPAGATION SECTION ****************************************************
  # loop over the number of runs
  for irun in range(n_runs):
    tstart = time()
    for iz0, z0 in enumerate(z0_grid):
      if F_speak: print( 'Doing %d out of %d.' % (iz0+1,z0_pts))
      # initialize
      tset = class_trajectories(n_traj,F_eff_r=F_eff_r)
      tset.initialize(sdat,xyz0=z0)
      # ionize
      tset.ionize(sdat)
      # count trajectories
      counter[iz0] += tset.count_trajectories(theta_detect)
      # kill escaped trajectories
      tset.kill_escaped()
      
      while tset.g_n_alive:
        # scatter
        tset.scatter(sdat)
        # count trajectories
        counter[iz0] += tset.count_trajectories(theta_detect)
        # kill escaped trajectories
        tset.kill_escaped()
    tend = time()
    print('Done step %d out of %d in %10.2f seconds' % (irun+1,n_runs,tend-tstart))
    
  # fitting to determine the EAL
  fitres, fiterr = [], []
  for ii in range(nt):
    counter_now = counter[:,ii] / counter[:,ii].max()            # normalize
    [fres, ferr] = curve_fit(tools.eal, -z0_grid, counter_now, fit_guess)
    fitres.append(fres)
    fiterr.append(ferr)
  
    if plot_name:
      z0_grid_interp = -np.linspace(z0_grid[0],z0_grid[-1],1000)
      plt.figure()
      plt.plot(-z0_grid, counter_now,'o')
      plt.plot(z0_grid_interp,tools.eal(z0_grid_interp,*fres))
      plt.savefig('eal_%s_%d.png' % (plot_name,ii))
      plt.close()
    
  return z0_grid, np.squeeze(counter), np.squeeze(fitres), np.squeeze(fiterr)

def compute_eal_tilted_surface(gamma,
                d0_min,d0_max,d0_pts,n_traj,sdat,theta_detect,n_runs=1,
                F_speak=False,plot_name=False,fit_guess=np.array([1.,1.])):
  '''
    Convenience function to compute the EAL (effective attenuation length) for 
    a tilted surface. Input should be in atomic units.
    The initial position is sampled linearly between a depth of d0_min and 
    d0_max with d0_pts points, for a surface tilted by an angle gamma. 
    
    Input:
      gamma .......... tilting angle of surface (gamma=0 means z=0 is surface,
                       any other gamma tilts around x axis)
      d0_min ......... minimum starting depth in bohr
      d0_max ......... maximum starting depth in bohr
      d0_pts ......... number of points for the starting depth
      n_traj ......... number of trajectories per run
      sdat ........... system data (instance of class_system)
      theta_detect ... detection angle (number, numpy array or None (count all trajectories))
      n_runs ......... number of runs
      F_speak ........ flag for some printed comments
      plot_name ...... false for not plotting, otherwise filename to store plot
    
    Returns:
      d0_grid ........ grid of initial depts
      counter ........ (weighted) counted trajectories for each detection angle
      fitres ......... result of the fit to tools.eal
      fiterr ......... covariance matrix of the fit to tools.eal
  '''
  # *** INITIALIZATION SECTION *************************************************
  # grid of initial positions
  d0_grid = np.linspace(d0_min,d0_max,d0_pts)
  
  # prepare counter for exited electrons
  if hasattr(theta_detect, "__len__"): 
    nt = len(theta_detect)
  else: 
    nt = 1
  counter = np.zeros([d0_pts,nt])

  # *** PROPAGATION SECTION ****************************************************
  # loop over the number of runs
  for irun in range(n_runs):
    tstart = time()
    for id0, d0 in enumerate(d0_grid):
      if F_speak: print( 'Doing %d out of %d.' % (id0+1,d0_pts))
      # initialize
      tset = class_trajectories(n_traj)
      tset.initialize(sdat,gamma=gamma,xyz0=np.array([0,-d0*np.sin(gamma),d0*np.cos(gamma)]))
      # ionize
      tset.ionize(sdat)
      # count trajectories
      counter[id0] += tset.count_trajectories(theta_detect)
      # kill escaped trajectories
      tset.kill_escaped()
      
      while tset.g_n_alive:
        # scatter
        tset.scatter(sdat)
        # count trajectories
        counter[id0] += tset.count_trajectories(theta_detect)
        # kill escaped trajectories
        tset.kill_escaped()
    tend = time()
    print('Done step %d out of %d in %10.2f seconds' % (irun+1,n_runs,tend-tstart))
    
  # fitting to determine the EAL
  fitres, fiterr = [], []
  for ii in range(nt):
    counter_now = counter[:,ii] / counter[:,ii].max()            # normalize
    [fres, ferr] = curve_fit(tools.eal, -d0_grid, counter_now, fit_guess)
    fitres.append(fres)
    fiterr.append(ferr)
  
    if plot_name:
      d0_grid_interp = -np.linspace(d0_grid[0],d0_grid[-1],1000)
      plt.figure()
      plt.plot(-d0_grid, counter_now,'o')
      plt.plot(d0_grid_interp,tools.eal(d0_grid_interp,*fres))
      plt.savefig('eal_%s_%d.png' % (plot_name,ii))
      plt.close()
    
  return d0_grid, np.squeeze(counter), np.squeeze(fitres), np.squeeze(fiterr)

def extrapolate_emfp_eal(sdat,eal_target,emfp,emfp_acc,imfp_f,z0_min,z0_max,z0_pts,
                         n_traj_eal,ang_det_eal,n_runs_eal,nmult=5,F_sym=True,
                         plot_name=False,F_eff_r=0):
  '''
    Given is a target value for the EAL (effective attenuation length) as well 
    as  an initial guess for the EMFP (elastic mean free path) and IMFP/EMFP 
    (average number of elastic scatterings, with IMFP being the inelastic mean 
    free path). This function updates the EMFP to get a value of the EAL 
    which is closer to the target by linear interpolation. 
    
    Input:
      sdat ........... system data (instance of class_system)
      eal_target ..... target value for EAL
      emfp ........... initial guess of the EMFP
      emfp_acc ....... accuracy of the desired EMFP (this is not the accuracy 
                       of the EMFP that this function returns but the accuracy
                       that is obtained by calling this function until 
                       convergence); if there is a problem, chose a larger value 
                       of emfp_acc to get a better initial guess for emfp
      imfp_f ......... IMFP/EMFP
      z0_min ......... minimum starting position (maximum depth, less than 0; in bohn)
      z0_max ......... maximum starting position (minimum depth, less than 0; in bohn)
      z0_pts ......... number of points for the starting position
      n_traj_eal ..... number of trajectories to be used
      ang_det_eal .... detection angle (number, numpy array or None (count all trajectories))
      n_runs_eal ..... number of runs (total number of trajectories is 
                       n_traj_eal*n_runs_eal; this is used to save memory, as 
                       for each run only n_traj_eal are kept in memory)
      nmult .......... in case there is a problem, a test calculation is done 
                       with accuracy emfp_acc*nmult [default: nmult=5]
      F_sym .......... flag to chose if the two test calculations for the 
                       linear interpolation of the emfp should be symmetrical 
                       w.r.t. the initial emfp (F_sym=True) or if it is 
                       between the initial guess of emfp and emfp + emfp_acc
                       (F_sym=False) [default: F_sym=True]
    Returns:
      emfp ........... updated value of the EMFP
  '''
  
  if F_sym:
    emfp_0 = emfp - emfp_acc/2.
    emfp_1 = emfp + emfp_acc/2.
  if not F_sym or emfp_0<0:
    emfp_0 = emfp
    emfp_1 = emfp + emfp_acc
  
  # first run
  sdat.emfp = emfp_0
  sdat.imfp = imfp_f * sdat.emfp
  z0, counter, fitres, fiterr = compute_eal(z0_min,z0_max,z0_pts,n_traj_eal,sdat,
                                            ang_det_eal,n_runs_eal,plot_name=plot_name,
                                            F_eff_r=F_eff_r)
  eal_0 = fitres[1]
  # second run
  sdat.emfp = emfp_1
  sdat.imfp = imfp_f * sdat.emfp
  z0, counter, fitres, fiterr = compute_eal(z0_min,z0_max,z0_pts,n_traj_eal,sdat,
                                            ang_det_eal,n_runs_eal,plot_name=plot_name,
                                            F_eff_r=F_eff_r)
  eal_1 = fitres[1]
  # check: PAD computed with larger EMFP should always be larger
  if eal_1 < eal_0:
    print(' WARNING: Error while computing the EAL.                        ')
    print('          Trying once again. If it fails, try computation with  ')
    print('          more trajectories n_traj_eal or with different initial')
    print('          values.                                               ')
    # repeat calculation with significantly larger step size
    emfp_1 = emfp + nmult * emfp_acc
    sdat.emfp = emfp_1
    sdat.imfp = imfp_f * sdat.emfp
    z0, counter, fitres, fiterr = compute_eal(z0_min,z0_max,z0_pts,n_traj_eal,sdat,
                                              ang_det_eal,n_runs_eal,plot_name=plot_name,
                                              F_eff_r=F_eff_r)
    eal_1 = fitres[1]
    
  # linear interpolation to find the new EMFP
  emfp = emfp_0 + (eal_target-eal_0) * (emfp_1-emfp_0) / (eal_1-eal_0)
  
  # print results of first run (not the latest value, but it saves computing time 
  # and still shows the convergence)
  print(' EMFP: %8.3f nm, IMFP/EMFP: %8.3f, eal:  %8.3f nm' % (emfp_0/conv.nm_to_au,imfp_f,eal_0/conv.nm_to_au))
  
  return emfp


def extrapolate_imfp_eal(sdat,eal_target,emfp,imfp,imfp_acc,z0_min,z0_max,z0_pts,
                         n_traj_eal,ang_det_eal,n_runs_eal,nmult=5,F_sym=True,
                         plot_name=False,F_eff_r=0):
  
  '''
    Given is a target value for the EAL (effective attenuation length) as well 
    as  an initial guess for the IMFP (inelastic mean free path) and a value of 
    the EMFP (elastic mean free path). This function updates the IMFP to get a 
    value of the EAL which is closer to the target by linear interpolation. 
    
    Input:
      sdat ........... system data (instance of class_system)
      eal_target ..... target value for EAL
      emfp ........... value of the EMFP
      imfp ........... initial guess of the EMFP
      imfp_acc ....... accuracy of the desired IMFP (this is not the accuracy 
                       of the IMFP that this function returns but the accuracy
                       that is obtained by calling this function until 
                       convergence); if there is a problem, chose a larger value 
                       of emfp_acc to get a better initial guess for imfp
      z0_min ......... minimum starting position (maximum depth, less than 0; in bohn)
      z0_max ......... maximum starting position (minimum depth, less than 0; in bohn)
      z0_pts ......... number of points for the starting position
      n_traj_eal ..... number of trajectories to be used
      ang_det_eal .... detection angle (number, numpy array or None (count all trajectories))
      n_runs_eal ..... number of runs (total number of trajectories is 
                       n_traj_eal*n_runs_eal; this is used to save memory, as 
                       for each run only n_traj_eal are kept in memory)
      nmult .......... in case there is a problem, a test calculation is done 
                       with accuracy emfp_acc*nmult [default: nmult=5]
      F_sym .......... flag to chose if the two test calculations for the 
                       linear interpolation of the imfp should be symmetrical 
                       w.r.t. the initial imfp (F_sym=True) or if it is 
                       between the initial guess of imfp and imfp + emfp_acc
                       (F_sym=False) [default: F_sym=True]
    Returns:
      imfp ........... updated value of the IMFP
  '''
  
  # set the two <imfp_f> that will be used
  if F_sym:
    imfp_0 = imfp - imfp_acc/2.
    imfp_1 = imfp + imfp_acc/2.
  if not F_sym or imfp_0<0:
    imfp_0 = imfp
    imfp_1 = imfp + imfp_acc
  
  # first run
  sdat.emfp = emfp
  sdat.imfp = imfp_0
  z0, counter, fitres, fiterr = compute_eal(z0_min,z0_max,z0_pts,n_traj_eal,sdat,
                                            ang_det_eal,n_runs_eal,plot_name=plot_name,
                                            F_eff_r=F_eff_r)
  eal_0 = fitres[1]
  # second run
  sdat.emfp = emfp
  sdat.imfp = imfp_1
  z0, counter, fitres, fiterr = compute_eal(z0_min,z0_max,z0_pts,n_traj_eal,sdat,
                                            ang_det_eal,n_runs_eal,plot_name=plot_name,
                                            F_eff_r=F_eff_r)
  eal_1 = fitres[1]
  # check: PAD computed with larger EMFP should always be larger
  if eal_1 < eal_0:
    print(' WARNING: Error while computing the EAL.                        ')
    print('          Trying once again. If it fails, try computation with  ')
    print('          more trajectories n_traj_eal or with different initial')
    print('          values.                                               ')
    # repeat calculation with significantly larger step size
    imfp_1  = imfp_1 + nmult*imfp_acc
    sdat.emfp = emfp
    sdat.imfp = imfp_1
    z0, counter, fitres, fiterr = compute_eal(z0_min,z0_max,z0_pts,n_traj_eal,sdat,
                                              ang_det_eal,n_runs_eal,plot_name=plot_name,
                                              F_eff_r=F_eff_r)
    eal_1 = fitres[1]
    
  # linear interpolation to find the new IMFP
  imfp = imfp_0 + (eal_target-eal_0) * (imfp_1-imfp_0) / (eal_1-eal_0)
  
  # print results of first run (not the latest value, but it saves computing time 
  # and still shows the convergence)
  print(' EMFP: %8.3f nm, IMFP: %8.3f, eal:  %8.3f nm' % (emfp/conv.nm_to_au,imfp/conv.nm_to_au,eal_0/conv.nm_to_au))
  
  return imfp
