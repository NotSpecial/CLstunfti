'''
CLstunfti
by Axel Schild

This file is part of CLstunfti.

CLstunfti is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as 
published by the Free Software Foundation, either version 3 of 
the License, or any later version.

CLstunfti is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public 
License along with CLstunfti.  If not, see <http://www.gnu.org/licenses/>.


Load system data. There is a default file 'prop_data.h5' where all the data 
should be. Note that <phase_laes> is strange and is replaced by [-pi/2,0,pi/2].
'''
import numpy                               as np
import CLstunfti.conversion_factors        as conv
from   CLstunfti.tools import saveToHDF5, loadFromHDF5
from   scipy.integrate import cumtrapz     as ctrpz

class class_system:
  '''
  Class containing all information about the system. This information is 
  either generated from the input or loaded from a file.
  
  NOTE: All input is assumed to be in atomic units. Use the conversion factors
        from CLstunfti.conversion_factors to convert your input data. The only 
        input where this matters is the kinetic energy of the electron E_kin.
  
  NOTE: It is assumed that the differential cross sections are for a polar angle 
        ranging from 0 to pi.
  
  Operating modes:
    if   <infile>  is given:
      Assume the data was prepared beforehand and can be loaded from the file
      <infile>.
    elif <outfile> is given:
      Assume the data should be prepared and stored in the file <outfile>.
    else:
      Assume the data should be prepared and not stored at all.
  
  Input arguments:
    outfile ... string or None, name of output file (default: None)
    infile  ... string or None, name of  input file (default: None)
    
  If infile=None, the following information needed for the calculation with 
  CLstunfti have to be given:
    E_kin     ... kinetic energy of electron (atomic units)
                    type: float
    pdf_ion   ... differential ionization cross section, aka photoelectron 
                  angular distribution (any unit, without volume element)
                    type: numpy array, one dimension
    pdf_sca   ... differential scattering cross section; (any unit, without 
                  volume element)
                    type: numpy array, one dimension
  '''
  def __init__(self,outfile=None,infile=None,**kwargs):
    
    if not infile:
      # assume we have to compute everything
      self.test_sanity(**kwargs)                                                # test if input is sane
      self.k = np.sqrt(2*self.E)                                                 # momentum
      # ionization
      self.ang_ion    = np.linspace(0,np.pi,len(self.pdf_ion))                  # angular grid
      self.cdf_ion    = ctrpz(self.pdf_ion*np.sin(self.ang_ion),initial=0)      # cumulative DICS
      self.cdf_ion   /= self.cdf_ion[-1]                                        # normalize to draw random number from 0 to 1
      # scattering
      self.ang_sca    = np.linspace(0,np.pi,len(self.pdf_sca))                  # angular grid
      self.cdf_sca    = ctrpz(self.pdf_sca*np.sin(self.ang_sca),initial=0)      # cumulative DSCS
      self.cdf_sca   /= self.cdf_sca[-1]                                        # normalize to draw random number from 0 to 1
    
      if outfile:
        stuff = { 'ang_ion'      : self.ang_ion, 
                  'pdf_ion'      : self.pdf_ion,
                  'cdf_ion'      : self.cdf_ion,
                  'ang_sca'      : self.ang_sca,
                  'pdf_sca'      : self.pdf_sca,
                  'cdf_sca'      : self.cdf_sca,
                  'E'            : self.E,
                  'k'            : self.k      }
        saveToHDF5(outfile,stuff)
    
    else:
      # load provided propagation data
      data = loadFromHDF5(infile)
      self.ang_ion = data['ang_ion']
      self.pdf_ion = data['pdf_ion']
      self.cdf_ion = data['cdf_ion']
      self.ang_sca = data['ang_sca']
      self.pdf_sca = data['pdf_sca']
      self.cdf_sca = data['cdf_sca']
      self.E       = data['E']
      self.k       = data['k']
      
    return
  
  def test_sanity(self,**kwargs):
    
    print('')
    print(' Checking input (only if required variables exist):')
    
    try:    self.E = kwargs['E_kin']
    except: print('  <E_kin>   is missing.')
    try:    self.pdf_ion = kwargs['pdf_ion']
    except: print('  <pdf_ion> is missing.')
    try:    self.pdf_sca = kwargs['pdf_sca']
    except: print('  <pdf_sca> is missing.')
    
    print('')
    
    return
    
  
  def scattering_cdf_from_rdf(self,rmin,rmax,nr,rc,d,c):
    '''
    Scattering according to a radial distribution function g(r) = self.rdf_vals
    which is parametrized similarely to doi:10.1063/1.470654 and is computed 
    on the grid self.rdf_grid
    
    We need to compute R(r) = self.cdf_rdf by first computing the probability
    density of not having scattering until distance r by solving
      
      d/dt rho(r) = -g(r)/r_c
      
    with r_c = self.emfp (THIS IS NOT THE USUAL DEFINITION OF THE EMFP) and 
    subsequently computing 
      
      R(r) = int_0^r rho(s) ds
    
    NOTE: This function has to be called manually because it is not a general
          approach. You may provide your own class_system.rdf_grid and
          class_system.rdf_cdf instead.
    '''
    
    from scipy.integrate import odeint
    from scipy.integrate import cumtrapz as ctrpz
    
    def drho_dr(rho,r,rc,d,gd,m,c_lambda,c_alpha,c_beta,c_theta):
      '''
      Function for the radial distribution is inspired by doi:10.1063/1.470654.
      Experimental data need to be fitted to this function to provide the 
      input parameters m, c_lambda, c_alpha, c_beta, c_theta.
      Other parameters:
        d .... position of (first, global) maximum of the RDF (in atomic units)
        gd ... value of maximum of the RDF
      
      Example parameters for water (fit to doi:10.1063/1.470654):
        d        = 5.2912331487821733 a0
        gd       = 2.575158
        m        = 3.31059258
        c_lambda = 1.52793529
        c_alpha  = 7.45926124
        c_beta   = 11.47200799
        c_theta  = 198.6068651
      '''
      y = r/d
      if y > 1.:
        rdf = 1 + y**(-m) * (gd - 1. - c_lambda) \
                + ((y-1+c_lambda)/y) * np.exp(-c_alpha*(y-1)) * np.cos(c_beta*(y-1))
      else:
        rdf = gd * np.exp(-c_theta*(y-1)**2)
      return -rdf/rc * rho
    
    # set up grid
    self.rdf_grid = np.linspace(rmin,rmax,nr)
    self.rdf_del  = (rmax-rmin)/float(nr-1)
    dr = (rmax-rmin) / float(nr-1)
    
    # compute pdf
    rho = np.squeeze( odeint( drho_dr,
                              1.0,
                              self.rdf_grid,
                              args=(rc,c[0],c[1],c[2],c[3],c[4],c[5],c[6])
                              ) )
    rho /= np.sum(rho) * dr

    # compute CDFs
    self.rdf_pdf  = rho
    self.rdf_cdf  = ctrpz(rho,initial=0,dx=dr)
    self.rdf_cdf /= self.rdf_cdf[-1]
    return
