'''
CLstunfti
by Axel Schild

This file is part of CLstunfti.

CLstunfti is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as 
published by the Free Software Foundation, either version 3 of 
the License, or any later version.

CLstunfti is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public 
License along with CLstunfti.  If not, see <http://www.gnu.org/licenses/>.


Simple detector class that saves the results of the trajectory propagation.
'''
import numpy as np

class class_detector:
  def __init__(self,n,n_mult=1,amax=1.5707963267948966):
    self.size      = n                                     # number of detectors
    self.angle_max = float(amax)
    self.angle     = np.linspace(amax/n,amax,n)
    self.count     = np.zeros(n)
