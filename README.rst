####################
CLstunfti
####################

This is CLstunfti, an extendable Python toolbox to compute scattering of 
electrons with a given kinetic energy in liquids. It uses a continuum 
trajectory model with differential ionization and scattering cross sections as 
input to simulate the motion of the electrons through the medium. 
*CLstunfti is developed and tested for LINUX systems only.*

Purpose
_______

CLstunfti was originally developed to simulate two experiments: A measurement 
of the effective attenuation length (EAL) of photoelectrons in liquid water [1]
and a measurement of the photoelectron angular distribution (PAD) of 
photoelectrons in liquid water [2]. These simulations were performed to 
determine the two central theoretical parameters of every simulation of 
electron scattering in liquids: The elastic mean free path (EMFP) and the 
inelastic mean free path (IMFP). As the code can also be used to simulate other 
liquids and amorphous solids and because it can be easily extended, it is 
released to the public.

[1] Suzuki et. al., Phys. Rev. E 90, 010302 (2014)

[2] Thürmer et. al., Phys. Rev. Lett. 111, 173005 (2013)


How to install CLstunfti
________________________

1) You need to have a fortran compiler installed, e.g., `gcc-fortran`.

   Then, run the following command to install CLstunfti

   ::
  
      pip install .

2)
  Build the documentation by running
  
  ::
      
      make html
  
  It is found in ``_build/html`` (actually, it should already be there).
  
3)
  Use the CLstunfti module!


What CLstunfti does and how to use it
_____________________________________

Any calculation of electron scattering in liquids using CLstunfti needs the 
following input:
  
- A photoelectron angular distribution (PAD) for ionization (i.e., the initial 
  angular distribution of the electron trajectories) on a grid. The PAD 
  depends only on the polar angle theta which provides the grid
- A differential scattering cross section (DSCS) given on a grid for the same 
  theta grid.
- A value for the elastic mean-free path (EMFP).
- A value for the inelastic mean-free path (IMFP).
- A kinetic energy of the electrons (eKE).
  
It then starts random trajectories according to the PAD for ionization.
The starting positions may be given explicitly as (x,y,z)-coordinates or just 
as z-coordinate, or may be chosen randomly according to a linear or an 
exponential distribution (importance sampling). By default, the surface is at 
z=0 and the liquid is at z<0.

For each trajectory, the following happens: 
A maximum distance :math:`r_{\rm max}` is chosen randomly according to :math:`e^{-r_{max}/{\rm IMFP}}`. 
If the trajectory reaches this distance, it is discarded, because we only want to 
detect electrons at the pre-defined eKE. The trajectory is then propagated by 
selecting a distance  :math:`r` randomly sampled according to :math:`e^-r/{\rm EMFP}` 
where the electron is assumed to scatter inelastically. New polar and azimuthal 
angles are determined according to the DSCS and a uniform distribution, respectively. 
This is repeated until the trajectory escapes the surface (then it is measured) or 
until its total path length is longer than :math:`r_{\rm max}` (then it is discarded).

Note: The EAL is defined by fitting the number of electrons arriving at the surface to 

:math:`P(z_0) ~ e^{-z/{\rm EAL}}`

where :math:`z_0` is the depth of the ionization site below the surface.

Note: The PAD usually has the functional form 

:math:`{\rm PAD}(\theta) ~ 1 + \beta P_2(\cos(\theta))`
  
with :math:`P_2` being the Legendre polynomial of second order. Hence, it is 
characterized by a single parameter :math:`\beta`.


Examples
________

*CLstunfti does not contain unit tests (yet), but the results of the example 
calculations are provided in the corresponding figures or text files for 
comparison. If the seed* ``np.random.seed(1)`` *is used for the random number 
generator in the beginning of the files, the results of the calculation should 
be identical to what is shown in the figures/text files.*

The examples are in the folder ``examples``.
Each example comes with a sample output which has the same name as the files 
created by the scripts, but appended with ``_ref``.
Some of the examples have a rather long runtime, as indicated below.
This is because the examples should also show what is needed to compute the 
relevant targets correctly. If a quick test is preferred, the number of 
trajectories can be decreased.

The following examples are provided (note that part of the code is in the file 
``tools_eal_pad.py`` in the ``example`` folder):

* Example 01 shows how to prepare an input for CLstunfti. It is run as
  ::
      
    python 01_create_input.py
  
  It will create the HDF5 file ``prop_data.h5`` which can be compared with the 
  reference file ``prop_data_ref.h5``.

* Example 02 shows how to compute the effective attenuation length (EAL),
  i.e., the effective/average depth from which photoelectrons are ionized.
  This is done by selecting many ionization depths :math:`z_0` and by fitting the 
  number of electrons detected outside the liquid to 
  
  :math:`P(z_0) \propto e^{-z_0/{\rm EAL}}`
  
  It is run as 
  ::
      
    python 01_02_compute_eal.py
  
  and creates ``02_eal.pdf`` which can be compared with ``02_eal_ref.pdf``.
  *The calculation takes ca. 1 minute* on a 3.40GHz CPU. 

* Example 03 shows how to compute the photoelectron angular distribution 
  (PAD) of electrons that leave the liquid after photoionization.
  This is done by rotating the PAD for photoionization (which simulates a 
  rotation of the laser used for ionization) away from its default direction 
  (the :math:`z`-axis, as the default is that :math:`z<0` is the liquid and 
  :math:`z=0` is the surface) and by detecting the number of electrons outside 
  the liquid depending on the polar angle :math:`\theta` of the rotation.
  The PAD has the functional form 
  
  :math:`{\rm PAD}(\theta) \propto 1 + \beta P_2(\cos(\theta))`
  
  where :math:`P_2` is the Legendre polynomial of second order. Hence, the PAD
  is fully characterized by the parameter :math:`\beta`.
  
  Two ways to do the calculation are provided. The first uses importance 
  sampling of the ionization depth with an exponential distribution, is run 
  with 
  ::
      
    python 03a_compute_pad.py
  
  and creates ``03a_pad.pdf`` which can be compared with ``03a_pad_ref.pdf``.
  *The calculation takes ca. 1 hour* on a 3.40GHz CPU. 
  
  The second way uses a linear sampling, where initial positions are added until 
  deeper and deeper in the liquid until no trajectories are leaving it anymore.
  It is run with 
  ::
      
    python 03b_compute_pad.py
  
  and creates ``03b_pad.pdf`` which can be compared with ``03b_pad_ref.pdf``.
  *The calculation takes a few hours* on a 3.40GHz CPU. 
  
* Example 04 shows how to find elastic and inelastic mean free paths if an 
  EAL and PAD are given. From an initial guess for the EMFP and IMFP, it 
  optimizes their values by comparing the calculated EAL and PAD with a 
  target EAL and PAD.
  It is run with 
  ::
      
    python 04_find_emfp_imfp.py
  
  and provides the terminal output given in ``04_find_emfp_imfp_output.txt``
  for comparison.
  *The calculation takes ca. 1 hour* on a 3.40GHz CPU. 

* Example 05 compares the angular distribution of photoelectrons after ionization, 
  one scattering, two scatterings, etc. in the bulk (no surface) with the known 
  solution. There are four parts. The calculations should be performed in the 
  right order because the results are saved to files.
  
  In the first part, the angular distribution of the electrons after up to nine 
  scatterings in the bulk without inelastic scattering is computed. 
  It is run with 
  ::
      
    python 05a_bulk_prep.py
  
  and creates ``05a_bulk.pdf`` and ``05a_bulk.h5`` which can be compared 
  with ``05a_bulk_ref.pdf`` and ``05a_bulk_ref.h5``, respectively.
  *The calculation takes ca. 1.5 hours* on a 3.40GHz CPU. 
  
  In the second part, results of the first part are compared with a convolution
  of the initial PAD with the DSCS and with doing the exact equivalent of the 
  convolution (the convolution only gives the exact result in 2D, in 3D it is 
  more complicated).
  It is run with 
  ::
      
    python 05b_compare_bulk_convolution.py
  
  and creates ``05b_compare_bulk_convolution.pdf`` which can be compared 
  with ``05b_compare_bulk_convolution_ref.pdf``.
  *The calculation takes a few seconds* on a 3.40GHz CPU. 
  
  In the third part, the angular distribution of the electrons after up to nine 
  scatterings is computed outside the surface. 
  It is run with 
  ::
      
    python 05c_surface.py
  
  and creates ``05c_surface.pdf`` which can be compared with ``05c_surface_ref.pdf``.
  *The calculation takes ca. 10 minutes* on a 3.40GHz CPU. 
  
  In the fourth part, the results of the first and third part are compared.
  It is run with 
  ::
      
    python 05d_comparison_bulk_surface.py
  
  and creates ``05d_comparison_bulk_surface.pdf`` which can be compared
  with ``05d_comparison_bulk_surface_ref.pdf``.
  *The calculation takes a few seconds* on a 3.40GHz CPU. 
  
  

Notes on the implementation
___________________________

The central class is ``class_trajectories`` in the file ``trajectories.py``. The 
documentation of this file should be read to see what options are available.
The class ``class_system`` contains the relevant data of the system under study.
The class ``class_detector`` is a convenience class to count the number of 
electrons which escape the liquid.
``tools.py`` contains a few tools, e.g. for loading and saving data.
``conversion_factors.py`` contains conversion factors.
There is also some Fortran code for speed efficiency which is in ``ftools.f95``.
Every source file contains extensive inline documentation.


