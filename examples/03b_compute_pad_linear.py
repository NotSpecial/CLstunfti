'''
CLstunfti
by Axel Schild

This file is part of CLstunfti.

CLstunfti is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as 
published by the Free Software Foundation, either version 3 of 
the License, or any later version.

CLstunfti is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public 
License along with CLstunfti.  If not, see <http://www.gnu.org/licenses/>.

  
Determine the PAD given an EMFP and IMFP.

  PAD  ... photoelectron angular distribution
  EMFP ... mean free path for   elastic scattering
  IMFP ... mean free path for inelastic scattering

Here we use a liner sampling, i.e., n_traj trajectories are started deeper 
and deeper in the liquid until no trajectories reach the surface anymore.

NOTE: Because for the computation of the PAD averaging over the initial
      positions is needed, this function will run a while. If you turn on 
      plotting, it wil continously update a plot in the directory such that 
      the convergence can be monitored.
'''
import numpy                                            as np
from   scipy.optimize         import curve_fit
import CLstunfti.conversion_factors                     as conv
import CLstunfti.tools                                  as tools
from   CLstunfti.system       import class_system
from   tools_eal_pad          import compute_pad_linear

# set a seed for reproducability of the test calculation
np.random.seed(1)

# *** USER INPUT SECTION *******************************************************
# input for CLstunfti should be in atomic units (and angles in radian), they 
# are converted below
F_plot           = True                 # flag for plotting
emfp_nm          = 0.3                  # EMFP in nm
imfp_factor      = 10                   # IMFP/EMFP
ion_ang_min_deg  = 0.                   # minimal considered ionization angle (in degree)
ion_ang_max_deg  = 90.                  # maximum considered ionization angle (in degree)
ion_ang_pts      = 5                    # number of ionization angles for PAD computation
z0_delta_nm      = 0.1                  # step size for initial position sampling (in nm)
n_traj           = int(1e6)             # number of trajectories for each initial position
n_runs           = 100                  # number of runs (total number of trajectories is 
                                        # n_traj * n_runs)
datafile         = 'prop_data.h5'       # name of data file for propagation
theta_detect_deg = np.arange(1,10,2)    # detector opening angle (in degree); set to None to count all trajectories,
                                        # use a numpy array for multiple angles NOTE: Python lists won't work!
# ******************************************************************************

# unit conversions
emfp         = emfp_nm          * conv.nm_to_au
ion_ang_min  = ion_ang_min_deg  * conv.deg_to_rad
ion_ang_max  = ion_ang_max_deg  * conv.deg_to_rad
z0_delta     = z0_delta_nm      * conv.nm_to_au
theta_detect = theta_detect_deg * conv.deg_to_rad

# load system data
sdat = class_system(infile=datafile)
sdat.emfp = emfp_nm * conv.nm_to_au
sdat.imfp = imfp_factor * sdat.emfp

# initialize counter
counter = np.zeros([ion_ang_pts,len(theta_detect)])

# prepare some plotting
if F_plot:
  import matplotlib
  matplotlib.use('Agg')
  import matplotlib.pyplot as plt
  from   matplotlib import cm
  ion_ang_grid_plot     = np.linspace(ion_ang_min,ion_ang_max,1000) # grid for plotting
  ion_ang_grid_plot_deg = ion_ang_grid_plot / conv.deg_to_rad
  colors = [ cm.spring(ii) for ii in np.linspace(0,0.8,len(theta_detect)) ]

for irun in range(n_runs):
  print( ' Doing run %d out of %d.' % (irun+1,n_runs))
  # do the calculation
  ion_ang_grid, counter, fitres, fiterr = compute_pad_linear(
                                                      counter,
                                                      ion_ang_min,
                                                      ion_ang_max,
                                                      ion_ang_pts,
                                                      z0_delta,
                                                      n_traj,
                                                      sdat,
                                                      theta_detect,
                                                      F_speak=False)

  ion_ang_grid_deg = ion_ang_grid / conv.deg_to_rad

  beta     = fitres[:,1]
  beta_err = np.sqrt(fiterr[:,1,1])
  
  # plot the results
  if F_plot:
    for ii in range(len(theta_detect)):
      plt.plot(ion_ang_grid_deg     ,counter[:,ii]/counter[:,ii].max()         ,'o',c=colors[ii])
      plt.plot(ion_ang_grid_plot_deg,tools.pad(ion_ang_grid_plot,*fitres[ii])      ,c=colors[ii],
              label=r'EMFP$ = %5.2f nm$, $\beta = %5.2f$, opening angle$ = %5.1f^{\circ}$' % (emfp_nm,beta[ii],theta_detect_deg[ii]))
    
    plt.title('average number of scatterings: %5.2f' % imfp_factor)
    plt.legend(loc=4)
    plt.xlabel('ionization angle')
    plt.ylabel('normalized distribution of counted trajectories')
    plt.axis([ion_ang_grid_deg[0],ion_ang_grid_deg[-1],-0.05,1.05])
    plt.plot([ion_ang_grid_deg[0],ion_ang_grid_deg[-1]],[0,0],'k',lw=0.3)
    plt.plot([ion_ang_grid_deg[0],ion_ang_grid_deg[-1]],[1,1],'k',lw=0.3)
    plt.savefig('03b_pad.pdf')
    plt.clf()
