'''
CLstunfti
by Axel Schild

This file is part of CLstunfti.

CLstunfti is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as 
published by the Free Software Foundation, either version 3 of 
the License, or any later version.

CLstunfti is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public 
License along with CLstunfti.  If not, see <http://www.gnu.org/licenses/>.

  
Look how the angular distribution changes with each scattering.

Step 1: Compute the angular distribution in the bulk using CLstunfti.
    
'''
import numpy                                            as np
import CLstunfti.conversion_factors                     as conv
import CLstunfti.tools                                  as tools
from   CLstunfti.system       import class_system
from   CLstunfti.trajectories import class_trajectories

# set a seed for reproducability of the test calculation
np.random.seed(1)

# *** USER INPUT SECTION *******************************************************
# input for CLstunfti should be in atomic units (and angles in radian), what is  
# given here is converted below...
F_plot           = True                 # flag for plotting
F_save           = True                 # flag for saving
emfp_nm          = 0.3                  # EMFP in nm
imfp_factor      = 1000000              # IMFP/EMFP (set large because we want to look at the bulk)
z0               = -100.                # some initial postion deep inside the liquid (in nm)
nsca             = 9                    # number of scatterings
n_traj           = int(1e7)             # number of trajectories
n_runs           = 100                  # number of runs to average over
tbin_pts         = 100                  # number of angles for binning
tbin_min         = 0.                   # minimum angle for binning
tbin_max         = np.pi                # maximum angle for binning
E_eV             = 20.                  # electron kinetic energy (in eV)
beta_gas         = 1.4                  # PAD parameter for ionization
beta_liq         = 0.5                  # expected PAD parameter measured outside the liquid
w                = 17. * np.pi / 180.   # parameter for differential scattering cross section
# ******************************************************************************

def gaussian(x,sdev):
  return np.exp(-x**2/(2*w**2))

# photoelectron angular distribution (PAD) for ionization
theta    = np.linspace(0,np.pi,1001)                                            # some angular grid
pdf_ion  = 1. + beta_gas/2. * ( 3 * np.cos(theta)**2 - 1 )                      # PAD

# differential scattering cross section (DSCS)
pdf_sca  = gaussian(theta,w)

# create system data
E         = E_eV * conv.eV_to_au
sdat      = class_system(E_kin=E,pdf_ion=pdf_ion,pdf_sca=pdf_sca)
sdat.emfp = emfp_nm * conv.nm_to_au
sdat.imfp = imfp_factor * sdat.emfp

# some preparation
z0       *= conv.nm_to_au 
tbin_edges = np.linspace(tbin_min,tbin_max,tbin_pts)
tbin       = tbin_edges[:-1] + np.pi/(2*float(tbin_pts-1))
pad_av     = np.zeros([nsca+1,tbin_pts-1])  # array for PAD averaged over the runs

# compute gas-phase and liquid PADs for reference
P2      = (3*np.cos(tbin)**2-1.) / 2.
pad_gas = 1 + beta_gas*P2
pad_liq = 1 + beta_liq*P2
pad_gas /= pad_gas.max()
pad_liq /= pad_liq.max()


if F_plot:
  import matplotlib
  matplotlib.use('Agg')
  import matplotlib.pyplot as plt
  from   matplotlib import cm
  colors = [ cm.spring(ii) for ii in np.linspace(0,1,nsca+1) ]

# loop over the runs
for irun in range(n_runs):
  
  # preparation
  pad  = np.zeros([nsca+1,tbin_pts-1])
  tset = class_trajectories(n_traj,fixedF=1)
  tset.initialize(sdat,xyz0=z0)
  
  # ionization
  tset.ionize(sdat)
  tset.kill_escaped()
  
  # get the current polar angles and bin them
  pol_angle, _ = tools.cart2sph_ang(tset.dr[0],tset.dr[1],tset.dr[2])
  pad[0]       = np.histogram(pol_angle[tset.alive.astype(bool)],bins=tbin_edges)[0] / np.sin(tbin)
  #pad[0]      /= pad[0].max()   # possible normalization
  pad[0]      /= pad[0,0]        # better normalization
  
  for ii in range(nsca):
    print('  Doing scattering %d of %d.' % (ii+1,nsca))
    # scattering
    tset.scatter(sdat)
    tset.kill_escaped()
    
    pol_angle, _  = tools.cart2sph_ang(tset.dr[0],tset.dr[1],tset.dr[2])
    pad[ii+1]     = np.histogram(pol_angle[tset.alive.astype(bool)],bins=tbin_edges)[0] / np.sin(tbin)
    # pad[ii+1]    /= pad[ii+1].max()  # possible normalization
    pad[ii+1]    /= pad[ii+1,0]        # better normalization
    #print(' %d of 100' % (irun+1))
  
  pad_av += pad
  
  if F_plot:
    plt.clf()
    for ii in range(nsca+1): 
      plt.plot(tbin,pad_av[ii]/(irun+1),c=colors[ii])
    
    plt.plot(tbin,pad_gas,label='gas',c='gray')
    plt.plot(tbin,pad_liq,label='liq',c='k')
    plt.axis([tbin_min,tbin_max,0,1.02])
    plt.legend()
    plt.xlabel(r'polar angle')
    plt.ylabel(r'PAD')
    plt.savefig('05a_bulk.pdf')
    
  print(' %d out of %d' % (irun+1,n_runs))

pad = pad_av / n_runs

if F_save:
  stuff = { 'tbin'     : tbin,
            'pad'      : pad,
            'pad_gas'  : pad_gas,
            'pad_liq'  : pad_liq}
  fname = '05a_bulk.h5'
  tools.saveToHDF5(fname,stuff)

