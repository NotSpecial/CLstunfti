'''
CLstunfti
by Axel Schild

This file is part of CLstunfti.

CLstunfti is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as 
published by the Free Software Foundation, either version 3 of 
the License, or any later version.

CLstunfti is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public 
License along with CLstunfti.  If not, see <http://www.gnu.org/licenses/>.


Create input for CLstunfti using a DSCS loaded from a file.

Remember that the input should be in atomic units. This is only relevant for 
the kinetic energy of the electron.
'''
import numpy                           as np
import CLstunfti.conversion_factors    as conv 
from   CLstunfti.system import class_system

def gaussian(x,sdev):
  return np.exp(-x**2/(2*sdev**2))

# kinetic energy of the electron (in hartree)
E = 20 * conv.eV_to_au

# photoelectron angular distribution (PAD) for ionization 
# taken from 10.1103/PhysRevLett.111.173005
beta_ion = 1.4                                                                  # beta parameter
theta    = np.linspace(0,np.pi,1001)                                            # some angular grid
pdf_ion  = 1. + beta_ion/2. * ( 3 * np.cos(theta)**2 - 1 )                      # PAD

# differential scattering cross section (DSCS) 
# taken from 10.1103/PhysRevLett.111.173005
#w        = .1 * np.pi / 180.
w        = 17 * np.pi / 180.
pdf_sca  = gaussian(theta,w)

# compute data
outfile = 'prop_data.h5'
sdat = class_system(outfile=outfile,
                    E_kin=E,
                    pdf_ion=pdf_ion,
                    pdf_sca=pdf_sca)

