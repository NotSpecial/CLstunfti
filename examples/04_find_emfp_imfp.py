'''
CLstunfti
by Axel Schild

This file is part of CLstunfti.

CLstunfti is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as 
published by the Free Software Foundation, either version 3 of 
the License, or any later version.

CLstunfti is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public 
License along with CLstunfti.  If not, see <http://www.gnu.org/licenses/>.

  
  EAL  ... electron attenuation length
  PAD  ... photoelectron angular distribution
  EMFP ... mean free path for   elastic scattering
  IMFP ... mean free path for inelastic scattering

  PAD = 1. + beta/2. * ( 3 * np.cos(theta)**2 - 1 )

Aim of this script is to calculate the EMFP and IMFP for liquid water. This 
is done by computing the EAL and the PAD after the electrons have left the 
liquid. The necessary input is the beta parameter (see above) that determines 
the PAD for ionization, as well as the differential scattering cross section 
at the considered electron kinetic energy.

Also, target values for the EAL and for the beta that determines the PAD 
after the electron has left the liquid is needed. Then, the script optimized 
the EMFP and the ration IMFP/EMFP to match these target values.

NOTE 1: The script assumes that beta gets smaller with increasing IMFP/EMFP
        and that the EAL gets larger with increasing EMFP. This should be the 
        case. If not, there will be a WARNING. Typically, this fails for beta
        if the number of trajectories <n_traj_pad> for its computation or the
        target accuracy <imfp_f_acc> are too small.
NOTE 2: If you don't have good initial values for <emfp> (the EMFP) or 
        <imfp_f> (IMFP/EMFP), do not set the target accuracies <emfp_acc>
        and <imfp_f_acc> too small. <emfp_acc>=0.1 and <imfp_f_acc>=1.0 are 
        sensible values. The algorithm should converge within ca. 5 steps.
NOTE 3: Calculation of the EAL typically needs 10^5 trajectories. For the 
        calculation of beta we also need to average over the initial 
        positions, hence there typically 10^7 trajectories are necessary.
        Thus, the calculation of beta takes a rather long time, while the 
        calculation of the EAL is much faster.

How does it work?
  Step 1: Calculate beta for the initial guess of <emfp> and for the initial
          <imfp_f> as well as for <imfp_f> + <imfp_f_acc>. Get a new value of 
          <imfp_f> by linear inter-/extrapolation to <beta_target>.
  Step 2: Calculate EAL for the value of <imfp_f> and for <emfp> as well as 
          for <emfp> + <emfp_acc>. Get a new value for <emfp> by linear 
          inter-/extrapolation to <eal_target>.
  Then, repeat steps 1 & 2 until the change in <emfp> is smaller than 
  <emfp_acc> and the change in <imfp_f> is smaller than <imfp_f_acc>.
  
After each step, the information is saved to a file.
'''

import numpy                                            as np
from   scipy.optimize         import curve_fit
import CLstunfti.conversion_factors                     as conv
import CLstunfti.tools                                  as tools
from   CLstunfti.system       import class_system
from   tools_eal_pad          import extrapolate_imfp_f_pad, extrapolate_emfp_eal, compute_pad, compute_eal

# set a seed for reproducability of the test calculation
np.random.seed(1)

# *** USER INPUT SECTION *******************************************************
# input for CLstunfti should be in atomic units (and angles in radian), what is  
# given here is converted below...
# taget values
eal_nm_target = 1.8                     # EAL to optimize (in nm)
beta_target   = 0.5                     # beta (for liquid) to optimize
# input parameters
beta_ion      = 1.5                     # beta for ionization
E_eV          = 20.                     # electron kinetic energy (in eV)
w             = 17. * np.pi / 180.      # parameter for differential scattering cross section
# convergence parameters
emfp_nm       = 0.3                     # initial value for EMFP (in nm)
emfp_nm_acc   = 0.01                    # accuracy for EMFP (in nm)
imfp_f        = 5                       # initial value of IMFP/EMFP
imfp_f_acc    = 1.0                     # accuracy for IMFP/EMFP
# EAL section
z0_nm_max    = -0.5                     # minimal considered starting position in nm (should be less than 0)
z0_nm_min    = -5.                      # maximal considered starting position in nm (should be less than 0)
z0_pts       = 12                       # number of starting depths for EAL computation
n_traj_eal   = int(1e6)                 # number of trajectories
ang_det_eal  = 5.*np.pi/180.            # detector opening angle; set to None to count all trajectories
# PAD section
ia_min  = 0.                            # minimal considered ionization angle
ia_max  = np.pi/2.                      # maximum considered ionization angle
ia_pts  = 5                             # number of ionization angles for PAD computation
z0_nm_scale  = 5.0                      # scale for sampling of initial positions (should be at least EAL in nm)
n_traj_pad   = int(1e6)                 # number of trajectories (n_runs_pad * n_traj_pad has to be relatively large)
n_runs_pad   = 10                       # numer of runs
ang_det_pad  = 5.*np.pi/180.            # detector opening angle; set to None to count all trajectories
# ******************************************************************************

def gaussian(x,sdev):
  return np.exp(-x**2/(2*w**2))

# conversions to atomic units
eal_target = eal_nm_target * conv.nm_to_au
emfp       = emfp_nm       * conv.nm_to_au
emfp_acc   = emfp_nm_acc   * conv.nm_to_au
z0_min     = z0_nm_min     * conv.nm_to_au
z0_max     = z0_nm_max     * conv.nm_to_au
z0_scale   = z0_nm_scale   * conv.nm_to_au

# photoelectron angular distribution (PAD) for ionization
theta    = np.linspace(0,np.pi,1001)                                            # some angular grid
pdf_ion  = 1. + beta_ion/2. * ( 3 * np.cos(theta)**2 - 1 )                      # PAD

# differential scattering cross section (DSCS)
pdf_sca  = gaussian(theta,w)

# create system data
E    = E_eV * conv.eV_to_au
sdat = class_system(E_kin=E,pdf_ion=pdf_ion,pdf_sca=pdf_sca)

# print some information
print(' Input:')
print('  beta for ionization   : %8.3f   ' % (beta_ion))
print('  electon kinetic energy: %8.3f eV' % (E_eV)    )
print('  initial EMFP          : %8.3f nm' % (emfp_nm) )
print('  initial IMFP/EMFP     : %8.3f   ' % (imfp_f)  )

# arrays to store convergence
R_pad_imfp_f = [imfp_f]
R_eal_emfp   = [emfp]

# test values for convergence
del_imfp_f = np.abs(2*imfp_f_acc)
del_emfp   = np.abs(2*emfp_acc)
print(' Targets:')
print('  eal                   : %8.3f nm' % (eal_nm_target))
print('  beta                  : %8.3f   ' % (beta_target  ))
print(' Convergence criteria:')
print('  EMFP                  : %8.3f nm' % (emfp_nm_acc))
print('  IMFP/EMFP             : %8.3f   ' % (imfp_f_acc))
print('')

# start convergence run
irun = 0
while (del_imfp_f>imfp_f_acc) or (del_emfp>emfp_acc):
  irun += 1
  
  # get new estimate of imfp_f using the PAD
  imfp_f = extrapolate_imfp_f_pad(sdat,beta_target,emfp,imfp_f,imfp_f_acc,ia_min,ia_max,ia_pts,
                                  z0_scale,n_traj_pad,ang_det_pad,n_runs_pad)
  
  R_pad_imfp_f.append(imfp_f)
  # update test value for convergence
  del_imfp_f = np.abs(R_pad_imfp_f[-1]-R_pad_imfp_f[-2])
  # print convergence
  print(' del: %8.3f, step: %d' % (del_imfp_f,irun))
  
  # get new estimate of emfp using the EAL
  emfp = extrapolate_emfp_eal(sdat,eal_target,emfp,emfp_acc,imfp_f,z0_min,z0_max,z0_pts,
                              n_traj_eal,ang_det_eal)
  
  R_eal_emfp.append(emfp)
  # update test value for convergence
  del_emfp = np.abs(R_eal_emfp[-1]-R_eal_emfp[-2])
  # print convergence
  print(' del: %8.3f, step: %d' % (del_emfp/conv.nm_to_au,irun))
  
  # save current data
  stuff = { 'emfp_nm_final'          : emfp/conv.nm_to_au,
            'imfp_factor_final'      : imfp_f,
            'eal_nm_target'          : eal_nm_target,
            'beta_target'            : beta_target,
            'beta_ion'               : beta_ion,
            'eEkin_eV'               : E_eV,
            'emfp_nm_acc'            : emfp_nm_acc,
            'imfp_f_acc'             : imfp_f_acc,
            'emfp_nm_convergence'    : np.array(R_eal_emfp)/conv.nm_to_au,
            'imfp_factor_convergence': np.array(R_pad_imfp_f) }
  
  fname = '04_find_emfp_imfp.h5'
  tools.saveToHDF5(fname,stuff)

print(' Converged. Starting calculation of final values.')
# after convergence, we should compute the EAL and beta for the final values 
# of the EMFP and IMFP/EMFP, to compare
sdat.emfp = emfp
sdat.imfp = imfp_f * sdat.emfp
ia_grid, counter, fitres, fiterr = compute_pad(ia_min,ia_max,ia_pts,z0_scale,n_traj_pad,sdat,ang_det_pad,n_runs_pad)
beta = fitres[1]
z0_nm  , counter, fitres, fiterr = compute_eal(z0_min,z0_max,z0_pts,n_traj_eal,sdat,ang_det_eal)
eal  = fitres[1]

print('')
print(' Final values:')
print('  EMFP      = %8.3f nm' % (emfp/conv.nm_to_au  ))
print('  IMFP/EMFP = %8.3f   ' % (imfp_f              ))
print('  EAL       = %8.3f nm' % (eal/conv.nm_to_au   ))
print('  beta      = %8.3f   ' % (beta                ))
print(' Targets:')
print('  eal       = %8.3f nm' % (eal_nm_target ))
print('  beta      = %8.3f   ' % (beta_target   ))

# save final data
stuff = { 'emfp_nm_final'          : emfp/conv.nm_to_au,
          'imfp_factor_final'      : imfp_f,
          'eal_nm_target'          : eal_nm_target,
          'beta_target'            : beta_target,
          'beta_ion'               : beta_ion,
          'eEkin_eV'               : E_eV,
          'emfp_nm_acc'            : emfp_nm_acc,
          'imfp_f_acc'             : imfp_f_acc,
          'emfp_nm_convergence'    : np.array(R_eal_emfp)/conv.nm_to_au,
          'imfp_factor_convergence': np.array(R_pad_imfp_f) }
fname = '04_find_emfp_imfp.h5'
tools.saveToHDF5(fname,stuff)


