'''
CLstunfti
by Axel Schild

This file is part of CLstunfti.

CLstunfti is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as 
published by the Free Software Foundation, either version 3 of 
the License, or any later version.

CLstunfti is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public 
License along with CLstunfti.  If not, see <http://www.gnu.org/licenses/>.


Look how the angular distribution changes with each scattering.

Step 4: Compare the results with and without surface.
'''

import numpy as np
import matplotlib.pyplot as plt
import CLstunfti.tools as tools
from matplotlib import cm
plt.ion()

# *** load data from the bulk calculation
data = tools.loadFromHDF5('05a_bulk.h5')
n_sca = len(data['pad'])

colors = [ cm.spring(ii) for ii in np.linspace(0,1,22) ]

# plot it
#colors = [ cm.spring(ii) for ii in np.linspace(0,1,n_sca) ]
plt.figure(figsize=(6,4.5))
plt.subplot(1,2,1)
plt.title('bulk')
for ii in range(n_sca-1):
  plt.plot(data['tbin'],data['pad'][ii+1]/data['pad'][ii+1][0],c=colors[ii],lw=0.5)

plt.plot(data['tbin'],data['pad_gas'],':' ,c='gray',label='gas')
plt.plot(data['tbin'],data['pad_liq'],'-.',c='k'   ,label='liq')
plt.axis([0,np.pi/2.,0,1])
plt.xlabel('polar angle')
plt.ylabel('PAD (max. set to 1)')
plt.legend()


# *** load data from the surface calculation
data = tools.loadFromHDF5('05c_surface.h5')
for key,val in data.items(): exec(key + '=val')

ion_ang_min  = 0.                               # minimal considered ionization angle
ion_ang_max  = np.pi/2.                         # maximum considered ionization angle
ion_ang_grid_plot = np.linspace(ion_ang_min,ion_ang_max,1000) # grid for plotting
#colors = [ cm.spring(ii) for ii in np.linspace(0,1,len(beta)+1) ]


plt.subplot(1,2,2)
plt.title('with surface')
for ii in range(len(beta)-1):
  pad_now = tools.pad(ion_ang_grid_plot,*fitres[ii+1])
  plt.plot(ion_ang_grid_plot,pad_now/pad_now.max(),c=colors[ii+1],lw=0.5)

plt.axis([0,np.pi/2.,0,1])

ntb             = 100
theta_bin_edges = np.linspace(0,np.pi,ntb)
theta_bin       = theta_bin_edges[:-1] + np.pi/(2*float(ntb-1))

P2      = (3*np.cos(theta_bin)**2-1.) / 2.
pad_gas = 1 + data['beta_gas']*P2
pad_liq = 1 + data['beta_liq']*P2
pad_gas /= pad_gas.max()
pad_liq /= pad_liq.max()

plt.plot(theta_bin,pad_gas,':' ,label='gas',c='gray')
plt.plot(theta_bin,pad_liq,'-.',label='liq',c='k')
plt.xlabel('polar angle')

plt.savefig('05d_comparison_bulk_surface.pdf',bbox_in_scahes='tight')
