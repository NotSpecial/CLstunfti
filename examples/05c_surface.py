'''
CLstunfti
by Axel Schild

This file is part of CLstunfti.

CLstunfti is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as 
published by the Free Software Foundation, either version 3 of 
the License, or any later version.

CLstunfti is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public 
License along with CLstunfti.  If not, see <http://www.gnu.org/licenses/>.


Look how the angular distribution changes with each scattering.

Step 3: Compute the angular distribution with a surface using CLstunfti.
  
'''
import numpy                                            as np
from   scipy.optimize         import curve_fit
import CLstunfti.conversion_factors                     as conv
import CLstunfti.tools                                  as tools
from   CLstunfti.system       import class_system
from   CLstunfti.trajectories import class_trajectories
from   tools_eal_pad          import compute_pad_exp_nsca

# set a seed for reproducability of the test calculation
np.random.seed(1)

# *** USER INPUT SECTION *******************************************************
# input for CLstunfti should be in atomic units (and angles in radian), what is  
# given here is converted below...
F_plot       = True                 # flag for plotting (plots in each of the n_runs runs, overwrites)
F_save       = True                 # flag for saving
n_sca        = 20                   # number of scatterings
emfp_nm      = 0.3                  # EMFP in nm
imfp_factor  = 1000000              # IMFP/EMFP
ion_ang_min  = 0.                   # minimal considered ionization angle
ion_ang_max  = np.pi/2.             # maximum considered ionization angle
ion_ang_pts  = 5                    # number of ionization angles for PAD computation
z0_scale_nm  = 5.0                  # scale for sampling of initial positions
n_traj       = int(1e6)             # number of trajectories (has to be relatively large, e.g. 1e7)
n_runs       = 20                   # number of runs
theta_detect = 5.*np.pi/180.        # detector opening angle; set to None to count all trajectories
E_eV         = 20.                  # electron kinetic energy (in eV)
beta_gas     = 1.4                  # PAD parameter for ionization
beta_liq     = 0.5                  # expected PAD parameter measured outside the liquid
w            = 17. * np.pi / 180.   # parameter for differential scattering cross section
# ******************************************************************************

# plotting
if F_plot:
  import matplotlib
  matplotlib.use('Agg')
  import matplotlib.pyplot as plt
  from   matplotlib import cm, colors
  colors = [ cm.spring(ii) for ii in np.linspace(0,1,n_sca+1) ]
  ion_ang_grid_plot = np.linspace(ion_ang_min,ion_ang_max,1000) # grid for plotting

# photoelectron angular distribution (PAD) /differential ionization cross section (DICS)
theta    = np.linspace(0,np.pi,1001)                                            # some angular grid
pdf_gas  = 1. + beta_gas/2. * ( 3 * np.cos(theta)**2 - 1 )                      # PAD
pdf_liq  = 1. + beta_liq/2. * ( 3 * np.cos(theta)**2 - 1 )                      # PAD

# differential scattering cross section (DSCS)
w        = 17. * np.pi / 180.
pdf_sca  = np.exp(-theta**2/(2*w**2))

# compute system data
E         = E_eV * conv.eV_to_au
sdat      = class_system(E_kin=E, pdf_ion=pdf_gas, pdf_sca=pdf_sca)
sdat.emfp = emfp_nm * conv.nm_to_au
sdat.imfp = imfp_factor * sdat.emfp

z0_scale  = z0_scale_nm * conv.nm_to_au

# preparing the counter
counter = np.zeros([ion_ang_pts,n_sca+1])

for i_run in range(n_runs):
  print(' Doing run %d out of %d.' % (i_run+1,n_runs))
  # do the calculation
  ion_ang_grid, counter, fitres, fiterr = compute_pad_exp_nsca( counter,
                                                                ion_ang_min,
                                                                ion_ang_max,
                                                                ion_ang_pts,
                                                                z0_scale,
                                                                n_traj,
                                                                sdat,
                                                                theta_detect,
                                                                n_sca       )
  
  
  beta     = fitres[:,1]
  beta_err = np.sqrt(fiterr[:,1,1])

  stuff = { 'ion_ang_grid' : ion_ang_grid,
            'counter'      : counter     ,
            'fitres'       : fitres      ,
            'fiterr'       : fiterr      ,
            'beta'         : beta        ,
            'beta_err'     : beta_err    ,
            'emfp'         : sdat.emfp   ,
            'imfp_factor'  : imfp_factor ,
            'beta_gas'     : beta_gas    ,
            'beta_liq'     : beta_liq    ,
            'nsca'         : n_sca       ,
            'eKE'          : E           }
  fname = '05c_surface.h5'
  tools.saveToHDF5(fname,stuff)

  # plotting
  if F_plot:
    plt.clf()
    for ii in range(len(beta)-1):
      pad_now = tools.pad(ion_ang_grid_plot,*fitres[ii+1])
      plt.plot(ion_ang_grid_plot,pad_now/pad_now.max(),c=colors[ii+1])
    
    plt.plot(theta,pdf_gas/pdf_gas.max(),label='gas',c='gray')
    plt.plot(theta,pdf_liq/pdf_liq.max(),label='liq',c='k')
    #plt.title('average number of scatterings: %5.2f' % imfp_factor)
    #plt.legend(loc=1)
    plt.xlabel('ionization angle')
    plt.ylabel('PAD (max. set to 1)')
    plt.axis([ion_ang_grid[0],ion_ang_grid[-1],-0.05,1.05])
    plt.plot([ion_ang_grid[0],ion_ang_grid[-1]],[0,0],'k',lw=0.3)
    plt.plot([ion_ang_grid[0],ion_ang_grid[-1]],[1,1],'k',lw=0.3)
    plt.savefig('05c_surface.pdf')
