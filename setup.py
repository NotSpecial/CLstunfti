'''
CLstunfti
by Axel Schild

This file is part of CLstunfti.

CLstunfti is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as 
published by the Free Software Foundation, either version 3 of 
the License, or any later version.

CLstunfti is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public 
License along with CLstunfti.  If not, see <http://www.gnu.org/licenses/>.

Description:
  File for compiling the Fortran code of the module. Run with
  
  python setup.py build_ext --inplace
'''

# Ensure that numpy is available for build, see:
# https://github.com/pypa/pip/issues/5761
from setuptools import dist
dist.Distribution().fetch_build_eggs(['numpy'])

from numpy.distutils.core import setup, Extension

ext = Extension(name='CLstunfti.ftools',sources =['CLstunfti/ftools.f95'])

setup(name        = 'ftools',
      version     = '1.00',
      description = "Fortran tools for CLstunfti",
      author      = "Axel Schild",
      ext_modules = [ext],
      install_requires = ["h5py", "scipy", "matplotlib", "numexpr", 'numpy'],
      packages=["CLstunfti"],
      )
